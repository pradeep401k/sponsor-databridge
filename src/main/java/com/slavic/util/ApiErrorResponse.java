package com.slavic.util;

import java.util.Date;

public class ApiErrorResponse {

    private int status;
    private int code;
    private String message;
    private String path;
    private Date timeStamp;

    public ApiErrorResponse(int status, int code, String message, String url, Date timeStamp) {
        this.status = status;
        this.code = code;
        this.message = message;
        this.path=url;
        this.timeStamp=timeStamp;
    }

    public int getStatus() {
        return status;
    }

    public int getCode() {
        return code;
    }

    public String getMessage() {
        return message;
    }
    
    

    public String getPath() {
		return path;
	}

	public void setPath(String path) {
		this.path = path;
	}

	public Date getTimeStamp() {
		return timeStamp;
	}

	public void setTimeStamp(Date timeStamp) {
		this.timeStamp = timeStamp;
	}

	@Override
    public String toString() {
        return "ApiErrorResponse{" +
                "status=" + status +
                ", code=" + code +
                ", message=" + message +
                ", path="+path+
                ", timeStamp="+timeStamp+
                '}';
    }
}