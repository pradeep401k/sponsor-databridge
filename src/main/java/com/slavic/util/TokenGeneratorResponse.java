package com.slavic.util;

import java.io.Serializable;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonInclude.Include;

public class TokenGeneratorResponse implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	@JsonInclude(Include.NON_NULL)
	private String success;
	@JsonInclude(Include.NON_NULL)
	private String referenceId;
	@JsonInclude(Include.NON_NULL)
	private String token;
	@JsonInclude(Include.NON_NULL)
	private String subject;
	@JsonInclude(Include.NON_NULL)
	private String issuer;
	@JsonProperty("errors")
	@JsonInclude(Include.NON_NULL)
	private ErrorInfo status;

	public String getSuccess() {
		return success;
	}

	public void setSuccess(String success) {
		this.success = success;
	}

	public String getReferenceId() {
		return referenceId;
	}

	public void setReferenceId(String referenceId) {
		this.referenceId = referenceId;
	}

	public String getToken() {
		return token;
	}

	public void setToken(String token) {
		this.token = token;
	}

	public ErrorInfo getStatus() {
		return status;
	}

	public void setStatus(ErrorInfo status) {
		this.status = status;
	}

	public String getSubject() {
		return subject;
	}

	public void setSubject(String subject) {
		this.subject = subject;
	}

	public String getIssuer() {
		return issuer;
	}

	public void setIssuer(String issuer) {
		this.issuer = issuer;
	}

}
