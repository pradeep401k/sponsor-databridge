package com.slavic.util;

import java.net.InetAddress;
import java.net.UnknownHostException;

import javax.servlet.http.HttpServletRequest;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class ClientIp {
	static Logger logger = LoggerFactory.getLogger(ClientIp.class);

	public ClientIp() {
		// TODO Auto-generated constructor stub
	}
	
	public static String getClientIp(HttpServletRequest request) {

        String remoteAddr = "";

        if (request != null) {
            remoteAddr = request.getHeader("X-FORWARDED-FOR");
            logger.debug("X-FORWARDED-FOR:"+remoteAddr );
            if (remoteAddr == null || "".equals(remoteAddr)) {
                remoteAddr = request.getRemoteAddr();
                logger.debug("get remote address:"+remoteAddr );
                if (remoteAddr.equalsIgnoreCase("0:0:0:0:0:0:0:1")) {
                    InetAddress inetAddress=null;
					try {
						inetAddress = InetAddress.getLocalHost();
					} catch (UnknownHostException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
                    String ipAddress = inetAddress.getHostAddress();
                    remoteAddr = ipAddress;
                }
            }
        }
        logger.debug("client ip address:"+remoteAddr );
        return remoteAddr;
    }

}
