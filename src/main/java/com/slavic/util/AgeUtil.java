package com.slavic.util;

import java.time.LocalDate;
import java.time.Period;

public class AgeUtil {

	 public static int getAgeFromDOB(LocalDate dob) {
		 return (Period.between(dob, LocalDate.now())).getYears();
	 }
}
