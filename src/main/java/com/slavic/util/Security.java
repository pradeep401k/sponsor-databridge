package com.slavic.util;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.security.core.context.SecurityContextHolder;

import com.slavic.springboot.constant.Constants;
import com.slavic.springboot.security.jwt.AuthenticatedUser;
import com.slavic.springboot.security.jwt.TokenAuthenticationService;

import io.jsonwebtoken.Jwts;

public class Security {
	
	private static final Logger log = LogManager.getLogger(Security.class);
			
	private static String secretKey = "9agd45L1Oubt0WP2dy8S4Q==";
	
	public static String getPeoId(HttpServletRequest request) {
		
		String token = request.getHeader("Authorization");
		String peoId = Jwts.parser()
        .setSigningKey(secretKey)
        .parseClaimsJws(token)
        .getBody().getIssuer();
		if(peoId != null && !peoId.isEmpty()) {
			return peoId;
		}else {
			return null;
		}
	}
	
	/**
	 * Method parcers a token and extracts a value from the subject variable
	 * @param request
	 * @return
	 */
  public static String getUsername(HttpServletRequest request) {
	
	  String token = request.getHeader("Authorization");
		String username = Jwts.parser()
      .setSigningKey(secretKey)
      .parseClaimsJws(token)
      .getBody().getSubject();
		if (!username.isEmpty() && username != null) {
			return username;
		}else {
			return null;
		}
	  
  }
	
	
	public static String getNewAccessToken(String token, String erId) {
		String userName= new TokenAuthenticationService().getSubject(token);
		String issuer = new TokenAuthenticationService().getIssuer(token);
		String newAccessToken = getAccessToken(userName+","+issuer, erId);
		return newAccessToken;
	}
	
	public static String getAccessToken(String passCode, String einTaxId) {
		String accessToken = new TokenAuthenticationService().getAccessToken(passCode, einTaxId); 
		SecurityContextHolder.getContext().setAuthentication(new AuthenticatedUser(passCode));
		return accessToken;
	}
	
	public static String getOptOutAccessToken(String token, String ssnDob) {
		String userName= new TokenAuthenticationService().getSubject(token);
		String issuer = new TokenAuthenticationService().getIssuer(token);
		String optOutAccessToken = getAccessToken(userName+","+issuer, ssnDob);
		return optOutAccessToken; //in this new access token issuer will have SSN and DOB combination with comma separated
	}
	
	public static String getErId(HttpServletRequest request, HttpServletResponse response) throws Exception {
		String token = request.getHeader(Constants.AUTH);
		String erId = new TokenAuthenticationService().getIssuer(token);
		String[] split = erId.split("[_]");
		erId = split[0];
		return erId;
	}
	
	public static String getId(HttpServletRequest request, HttpServletResponse response) throws Exception {
		String token = request.getHeader(Constants.AUTH);
		String id = new TokenAuthenticationService().getIssuer(token);	
		String[] split = id.split("[_]");
		if(split.length > 1) {
			id = split[1];
		}	
		return id;
	}
	
	public static String getAgentId(HttpServletRequest request, HttpServletResponse response) throws Exception {
		String token = request.getHeader(Constants.AUTH);
		String agentId = new TokenAuthenticationService().getSubject(token);			
		return agentId;
	}
	
	public static String[] getSsnDob(HttpServletRequest request, HttpServletResponse response) throws Exception {
		String token = request.getHeader(Constants.AUTH);
		String[] opt = new TokenAuthenticationService().getIssuer(token).split(",");
		return opt;
	}
	
	public static TokenGeneratorResponse sendDecodedTokenInfo(String token) {
		TokenGeneratorResponse decodedInfo = null;
		try {
			decodedInfo = new TokenGeneratorResponse();
			decodedInfo.setIssuer(new TokenAuthenticationService().getIssuer(token));
			decodedInfo.setSubject(new TokenAuthenticationService().getSubject(token));
		} catch (Exception e) {
			log.error("Token decoding went wrong" + e.getMessage());
		}
		return decodedInfo;
	}
}
