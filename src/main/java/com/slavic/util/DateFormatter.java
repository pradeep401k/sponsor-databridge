package com.slavic.util;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.Date;
import java.util.Locale;

import javax.xml.datatype.DatatypeConfigurationException;
import javax.xml.datatype.DatatypeFactory;
import javax.xml.datatype.XMLGregorianCalendar;

import org.apache.commons.lang3.StringUtils;

public class DateFormatter {

	public static Date formatDate(String strToDate) throws ParseException {
		Date date = null;
		if(StringUtils.isNotBlank(strToDate)) {
			DateFormat formatter = new SimpleDateFormat("E MMM dd HH:mm:ss Z yyyy",Locale.US); //input format
			date = (Date)formatter.parse(strToDate);
		}else
			return null;
		return date;
	}
	
	public static Date strToDate(String strToDate, String format) throws ParseException {
		Date date = null;
		if(StringUtils.isNotBlank(strToDate)) {
			SimpleDateFormat formatter = new SimpleDateFormat(format); //output format date
			date = (Date)formatter.parse(strToDate);
		}else
			return null;
		return date;
	}
	
	/**
	 * This is STRICTLY used by Post Beneficiary call to Relius while adding new Beneficiaries
	 * @param strToDate
	 * @param format
	 * @return
	 * @throws ParseException
	 */
	public static XMLGregorianCalendar strToXMLGregCal(String strToDate, String format) throws ParseException {
		XMLGregorianCalendar date = null;
		if(StringUtils.isNotBlank(strToDate)) {
			DateTimeFormatter originalDateFormatter = DateTimeFormatter.ofPattern(format);
			LocalDate tempDate = LocalDate.parse(strToDate,originalDateFormatter);
			try {
				date = DatatypeFactory.newInstance().newXMLGregorianCalendar(tempDate.toString()+"T00:00:00");
			} catch (DatatypeConfigurationException e) {
				return null;
			}
		}else
			return null;
		return date;
	}
}
