package com.slavic.util;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.text.NumberFormat;

public class NumberFormatter {

	
	public NumberFormatter() {
		
	}
	
	public static String formatNumberinUSD(BigDecimal bigDecimal ) {
		NumberFormat formatter = NumberFormat.getCurrencyInstance();
		return formatter.format(bigDecimal);
	}
	
	public static double roundNumber(double number, int decimals ) {
		 BigDecimal bd = new BigDecimal(number);
	     bd = bd.setScale(decimals, RoundingMode.HALF_UP);
		return bd.doubleValue();
	}
	
	public static String formatNumberinPercent(double percent) {
		
		return NumberFormat.getPercentInstance().format(percent);
	}
	
	
}
