package com.slavic.util;

import java.io.IOException;
import java.io.OutputStream;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.slavic.springboot.constant.Constants;

public class JsonResponseError {
	
	public static void setJsonResponseError(HttpServletRequest request, HttpServletResponse response, String message, String errorCode) throws IOException {
    	response.setContentType(MediaType.APPLICATION_JSON_VALUE);
        response.setStatus(HttpStatus.UNAUTHORIZED.value());
        response.setHeader("Access-Control-Allow-Origin", "*");
        Map<String, Object> data = new HashMap<>();
        data.put(Constants.TIMESTAMP, new Date());
        data.put(Constants.STATUS,HttpStatus.UNAUTHORIZED.value());
        data.put(Constants.ERRORMESSAGE, message);
        data.put(Constants.PATH, request.getRequestURL().toString());
        data.put(Constants.ERRORCODE, errorCode);
        OutputStream out = response.getOutputStream();
        com.fasterxml.jackson.databind.ObjectMapper mapper = new ObjectMapper();
        mapper.writeValue(out, data);
        out.flush();
    }
	
	public static void setJsonResponseError(HttpServletRequest request, HttpServletResponse response, String message) throws IOException {
		setJsonResponseError(request, response, message, "");
	}


}
