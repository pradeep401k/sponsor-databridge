package com.slavic.util;

public class AuthoritiesConstants {

	public static final String ADMIN = "ROLE_ADMIN";

	public static final String USER = "ROLE_USER";
	
	public static final String ANONYMOUS = "ROLE_ANONYMOUS";
	
	public static final String TPA_USER = "ROLE_TPA_USER";
	
	public static final String TPA_ADMIN = "ROLE_TPA_ADMIN";

	private AuthoritiesConstants() {
	}
}
