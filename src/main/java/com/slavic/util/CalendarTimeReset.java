package com.slavic.util;

import java.util.Calendar;
import java.util.Date;

public class CalendarTimeReset {

	public CalendarTimeReset() {
		// TODO Auto-generated constructor stub
	}
	
	public static Date getTime() {
		Calendar now = Calendar.getInstance();
        now.set(Calendar.HOUR, 0);
        now.set(Calendar.MINUTE, 0);
        now.set(Calendar.SECOND, 0);
        now.set(Calendar.HOUR_OF_DAY, 0);
        return now.getTime();
	}

}
