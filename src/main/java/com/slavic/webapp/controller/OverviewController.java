package com.slavic.webapp.controller;

import java.time.LocalDate;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.slavic.springboot.bean.StatusInfo;
import com.slavic.springboot.dto.AverageDeferralDTO;
import com.slavic.springboot.dto.ParticipantsDTO;
import com.slavic.springboot.service.AssetsService;
import com.slavic.springboot.service.AverageDeferralService;
import com.slavic.springboot.service.ParticipantService;
import com.slavic.springboot.service.PlansService;
import com.slavic.util.Security;

import io.swagger.annotations.ApiOperation;

@RestController
@RequestMapping("/overview")
public class OverviewController {

	
	@Autowired
	private AssetsService assetsService;
	
	@Autowired
	private PlansService planService;
	
	@Autowired
	private ParticipantService participantService;
	
	@Autowired
	private AverageDeferralService averageDeferralService;
	
	
	@GetMapping("/assets")
	@ApiOperation("Return overall assets for PEO based on time period given")
	public ResponseEntity<Object> getAssetsStats(@RequestParam("start") @DateTimeFormat(iso = DateTimeFormat.ISO.DATE)LocalDate start,
			@RequestParam("end") @DateTimeFormat(iso = DateTimeFormat.ISO.DATE)LocalDate end,
			@RequestParam(required = false) String interval, @RequestParam(required = false, defaultValue="0") int offset,
			@RequestParam(required = false, defaultValue="5") int limit, HttpServletRequest request, HttpServletResponse response){
		response.setHeader("Access-Control-Allow-Origin", "*");
		response.setHeader("Access-Control-Allow-Headers", "*");
		response.setHeader("Access-Control-Allow-Methods", "*");
		System.out.println("limit: "+limit+" offset: "+offset);
		
		String peoId=Security.getPeoId(request);
		
		if(peoId == null) {
			return ResponseEntity.badRequest().body(new StatusInfo("PEO id not found", "404"));
		}
		
		return ResponseEntity.ok().body(assetsService.getAssetsStats(peoId, start,  end, interval, limit));
	}
	
	@GetMapping("/plans")
	@ApiOperation("Return overall plan information for PEO based on time period given")
	public ResponseEntity<Object> getPlansStats(@RequestParam("start") @DateTimeFormat(iso = DateTimeFormat.ISO.DATE)LocalDate start,
			@RequestParam("end") @DateTimeFormat(iso = DateTimeFormat.ISO.DATE)LocalDate end,
			@RequestParam(required = false) String interval,
			@RequestParam(required = false, defaultValue="5") int limit,
			HttpServletRequest request, HttpServletResponse response){
		
		String peoId=Security.getPeoId(request);
		System.out.println("PLANS: "+peoId);
		if(peoId == null) {
			return ResponseEntity.badRequest().body(new StatusInfo("PEO id not found", "404"));
		}
		
	     response.setHeader("Access-Control-Allow-Origin", "*");
		
		return ResponseEntity.ok().body(planService.getPlansStats(peoId, start,  end, interval, limit));
	}
	
	@GetMapping("/participants")
	@ApiOperation("Return overall participant counts for PEO based on time period given")
	public ResponseEntity<ParticipantsDTO> getParticipantsStats(@RequestParam String start, @RequestParam String end,
			@RequestParam(required = false) String interval,
			@RequestParam(required = false, defaultValue="5") int limit,
			HttpServletRequest request, HttpServletResponse response){
		
		String peoId=Security.getPeoId(request);
		
	    response.setHeader("Access-Control-Allow-Origin", "*");
		
		return ResponseEntity.ok().body(participantService.getParticipantsStats(peoId,  LocalDate.parse(start), LocalDate.parse( end), interval, limit));
	}
	
	@GetMapping("/avg-deferral-rates")
	@ApiOperation("Return overall averge deferral rates for PEO based on time period given")
	public ResponseEntity<AverageDeferralDTO> getAvgDeferralRate(@RequestParam("start") @DateTimeFormat(iso = DateTimeFormat.ISO.DATE)LocalDate start,
			@RequestParam("end") @DateTimeFormat(iso = DateTimeFormat.ISO.DATE)LocalDate end,
					@RequestParam(required = false) String interval,
					@RequestParam(required = false, defaultValue="5") int limit,
					HttpServletRequest request, HttpServletResponse response){
		
		String peoId=Security.getPeoId(request);
		
	    response.setHeader("Access-Control-Allow-Origin", "*");
		
		return ResponseEntity.ok().body(averageDeferralService.getAverageDeferralStats(peoId, start, end, interval, limit));
	}
	
}
