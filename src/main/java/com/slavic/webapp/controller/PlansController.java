package com.slavic.webapp.controller;

import java.net.URISyntaxException;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.slavic.springboot.mysql.model.PlansChanges;
import com.slavic.springboot.mysql.model.PlansTotal;
import com.slavic.springboot.service.PlansService;
import com.slavic.util.Security;


@RestController
@RequestMapping("overview/webhooks/plans")
public class PlansController {

	@Autowired
	private PlansService plansService;
	
	
	 @PostMapping("/totals")
	    public ResponseEntity<PlansTotal> addTotalPlans(@RequestBody PlansTotal plansTotal ,HttpServletRequest request,
				HttpServletResponse response) throws URISyntaxException {
		   response.setHeader("Access-Control-Allow-Origin", "*");
	        //get the id for the step from ssn
			String user=Security.getUsername(request);
			//check user is valid for this call
			if(user.equals("sponsortest")) {
				//TODO: validate peoid is not NONE
				
				return ResponseEntity.ok().body(plansService.storePlansTotal(plansTotal));
			}
	     
	        //TODO: response for not authorized
	        return ResponseEntity.badRequest().body(null);
	    }
	
	 
	 @PostMapping("/changes")
	    public ResponseEntity<PlansChanges> addAssetsCnages(@RequestBody PlansChanges plansChanges,HttpServletRequest request,
				HttpServletResponse response) throws URISyntaxException {
	     
	        //get the id for the step from ssn
			String user=Security.getUsername(request);
			
			if(user.equals("sponsortest")) {
				return ResponseEntity.ok().body(plansService.storePlansChanges(plansChanges));
			}
	     
	        
	        return ResponseEntity.badRequest().body(null);
	    }
	
	
}
