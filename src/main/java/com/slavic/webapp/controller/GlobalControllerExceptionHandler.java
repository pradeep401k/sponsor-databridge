package com.slavic.webapp.controller;

import java.time.format.DateTimeParseException;
import java.util.Calendar;
import java.util.HashMap;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.MissingServletRequestParameterException;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestControllerAdvice;
import org.springframework.web.method.annotation.MethodArgumentTypeMismatchException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.convert.ConversionFailedException;
import org.springframework.core.env.Environment;
import org.springframework.http.HttpStatus;
import org.springframework.validation.FieldError;

import com.slavic.springboot.constant.Constants;
import com.slavic.util.ApiErrorResponse;


@RestControllerAdvice
public class GlobalControllerExceptionHandler{
	
	private final Logger log = LoggerFactory.getLogger(GlobalControllerExceptionHandler.class);
	
	@Autowired
	private Environment env;
	
	
	 @ResponseStatus(HttpStatus.BAD_REQUEST)
	 @ExceptionHandler(MethodArgumentTypeMismatchException.class)
	 public ApiErrorResponse handleDaMethodArgumentTypeMismatchExceptions(
			 MethodArgumentTypeMismatchException ex, HttpServletRequest req) {
		 log.error("MethodArgumentTypeMismatchException exception..."+ex.getMessage()); 
			ex.printStackTrace();
			return new ApiErrorResponse(Integer.parseInt(HttpStatus.BAD_REQUEST.toString()),Constants.BADREQUESTCODE,
					"Value "+ex.getValue()+" does not match expected format", req.getRequestURL()+"", Calendar.getInstance().getTime());

	      
	  }
	 
	 @ResponseStatus(HttpStatus.BAD_REQUEST)
	 @ExceptionHandler(MissingServletRequestParameterException.class)
	 public ApiErrorResponse handleMissingServletRequestParameterExceptions(
			 MissingServletRequestParameterException ex, HttpServletRequest req) {
		 log.error("MissingServletRequestParameterException exception..."+ex.getMessage()); 
			ex.printStackTrace();
			return new ApiErrorResponse(Integer.parseInt(HttpStatus.BAD_REQUEST.toString()),Constants.BADREQUESTCODE,
					ex.getMessage(), req.getRequestURL()+"", Calendar.getInstance().getTime());

	      
	  }

	
	@ExceptionHandler(Exception.class)
	@ResponseStatus(HttpStatus.INTERNAL_SERVER_ERROR)
	public ApiErrorResponse handleNotFoundException(Exception ex, HttpServletRequest req) {
		log.error("Unknown exception..."+ex.getMessage()); 
		ex.printStackTrace();
		String errorMsg=env.getProperty(Constants.FAILURECODE);
		return new ApiErrorResponse(Integer.parseInt(HttpStatus.INTERNAL_SERVER_ERROR.toString()),Integer.parseInt(Constants.FAILURECODE), errorMsg,  req.getRequestURL()+"", Calendar.getInstance().getTime());
	}
	
	 @ResponseStatus(HttpStatus.BAD_REQUEST)
	    @ExceptionHandler(MethodArgumentNotValidException.class)
	    public Map<String, String> handleValidationExceptions(
	      MethodArgumentNotValidException ex) {
	        Map<String, String> errors = new HashMap<>();
	        errors.put("error", "test");
	        ex.getBindingResult().getAllErrors().forEach((error) -> {
	            String fieldName = ((FieldError) error).getField();
	            String errorMessage = error.getDefaultMessage();
	            errors.put(fieldName, errorMessage);
	        });
	        return errors;
	    }
	 
	
}
