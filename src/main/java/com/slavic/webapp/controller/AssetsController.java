package com.slavic.webapp.controller;

import java.net.URISyntaxException;
import java.time.LocalDate;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.slavic.springboot.mysql.model.AssetsChanges;
import com.slavic.springboot.mysql.model.AssetsTotal;
import com.slavic.springboot.service.AssetsService;
import com.slavic.util.Security;


@RestController
@RequestMapping("/overview/webhooks/assets")
public class AssetsController {

	@Autowired
	private AssetsService assetsService;
	
	
	@GetMapping("/totals")
	public ResponseEntity<AssetsTotal> getAssetsStats(@RequestParam String start,
			HttpServletRequest request, HttpServletResponse response){
		
		String peoId=Security.getUsername(request);

		return ResponseEntity.ok().body(assetsService.getAssetsTotal(peoId, LocalDate.parse(start)));
	}
	
	 @PostMapping("/totals")
	    public ResponseEntity<AssetsTotal> addTotalAssets(@RequestBody AssetsTotal assetsTotal ,HttpServletRequest request,
				HttpServletResponse response) throws URISyntaxException {
		   response.setHeader("Access-Control-Allow-Origin", "*");
	        //get the id for the step from ssn
			String user=Security.getUsername(request);
			//check user is valid for this call
			if(user.equals("sponsortest")) {
				//TODO: validate peoid is not NONE
				
				return ResponseEntity.ok().body(assetsService.storeAssetsTotal(assetsTotal));
			}
	     
	        //TODO: response for not authorized
	        return ResponseEntity.badRequest().body(null);
	    }
	
	@GetMapping("/changes")
	public ResponseEntity<AssetsChanges> getAssetsStats(@RequestParam String start,@RequestParam String end,
			HttpServletRequest request, HttpServletResponse response){
		
//		String peoId=Security.getUsername(request);
		String peoId=Security.getPeoId(request);
		
		System.out.println("peoId: "+peoId);
		return ResponseEntity.ok().body(assetsService.getAssetsChange(peoId, LocalDate.parse(start), LocalDate.parse(end)));
	}
	
	 @PostMapping("/changes")
	    public ResponseEntity<AssetsChanges> addAssetsCnages(@RequestBody AssetsChanges assetsChanges,HttpServletRequest request,
				HttpServletResponse response) throws URISyntaxException {
	     
	        //get the id for the step from ssn
			String user=Security.getUsername(request);
			
			if(user.equals("sponsortest")) {
				return ResponseEntity.ok().body(assetsService.storeAssetChanges(assetsChanges));
			}
	     
	        
	        return ResponseEntity.badRequest().body(null);
	    }
	
	
	
}
