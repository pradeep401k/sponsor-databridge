package com.slavic.webapp.controller;

import java.net.URISyntaxException;
import java.time.LocalDate;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.slavic.springboot.mysql.model.AverageDeferral;
import com.slavic.springboot.service.AverageDeferralService;
import com.slavic.util.Security;

@RestController
@RequestMapping("/overview/webhooks/avg-deferrals")
public class AverageDeferralController {

	@Autowired
	private AverageDeferralService averageDeferralService;
	
	@GetMapping("/averages")
	public ResponseEntity<AverageDeferral> getAssetsStats(@RequestParam String start,@RequestParam String end,
			HttpServletRequest request, HttpServletResponse response){
		
		String peoId=Security.getPeoId(request);
		return ResponseEntity.ok().body(averageDeferralService.getAverageDeferral(peoId, LocalDate.parse(start), LocalDate.parse(end)));
	}
	
	 @PostMapping("/averages")
	    public ResponseEntity<AverageDeferral> addTotalAssets(@RequestBody AverageDeferral average ,HttpServletRequest request,
				HttpServletResponse response) throws URISyntaxException {
		   response.setHeader("Access-Control-Allow-Origin", "*");
	        //get the id for the step from ssn
			String user=Security.getUsername(request);
			//check user is valid for this call
			if(user.equals("sponsortest")) {
				//TODO: validate peoid is not NONE
				
				return ResponseEntity.ok().body(averageDeferralService.storeAverageDeferral(average));
			}
	     
	        //TODO: response for not authorized
	        return ResponseEntity.badRequest().body(null);
	    }
	
}
