package com.slavic.webapp.controller;

import java.net.URISyntaxException;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.slavic.springboot.mysql.model.ParticipantChangesEntity;
import com.slavic.springboot.service.ParticipantService;
import com.slavic.util.Security;


@RestController
@RequestMapping("/overview/webhooks/participants")
public class ParticipantsController {
	
	@Autowired
	ParticipantService partService;
	
	@PostMapping("/changes")
    public ResponseEntity<ParticipantChangesEntity> addTotalPlans(@RequestBody ParticipantChangesEntity participantChanges ,HttpServletRequest request,
			HttpServletResponse response) throws URISyntaxException {
	   response.setHeader("Access-Control-Allow-Origin", "*");
        //get the id for the step from ssn
		String user=Security.getUsername(request);
		//check user is valid for this call
		if(user.equals("sponsortest")) {
			//TODO: validate peoid is not NONE
			
			return ResponseEntity.ok().body(partService.storeParticipantChanges(participantChanges));
		}
     
        //TODO: response for not authorized
        return ResponseEntity.badRequest().body(null);
    }

}
