package com.slavic.webapp.config;

import java.util.Map;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.autoconfigure.web.servlet.MultipartAutoConfiguration;
import org.springframework.boot.web.servlet.FilterRegistrationBean;
import org.springframework.cache.CacheManager;
import org.springframework.cache.annotation.EnableCaching;
import org.springframework.cache.ehcache.EhCacheCacheManager;
import org.springframework.cache.ehcache.EhCacheManagerFactoryBean;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;
import org.springframework.core.io.ClassPathResource;

import com.slavic.springboot.helper.CacheHelper;
import com.slavic.webapp.CustomURLRewriter;
 

@Configuration
@ComponentScan(basePackages = {"com.slavic.datasource", "com.slavic.springboot", "com.slavic.service", "com.slavic.springboot.controllers", "com.slavic.webapp.controller"})
@PropertySource("classpath:sponsor-${environment.val}.properties")
@EnableCaching
//@EnableAutoConfiguration(exclude={MultipartAutoConfiguration.class})
public class AppConfig {
	
	@Value("${cache.location}")
	private String ehcacheLocation;
	
	@Value("${environment.val}")
	private String environment;

	
	//EhCache based CacheManager, most commonly used in Enterprise applications.
    @Bean
    public CacheManager cacheManager() {
        return new EhCacheCacheManager(ehCacheCacheManager().getObject());
    }
 
    @Bean
    public EhCacheManagerFactoryBean ehCacheCacheManager() {
        EhCacheManagerFactoryBean factory = new EhCacheManagerFactoryBean();
       // factory.setConfigLocation(new ClassPathResource("cache-config/ehcache-config.xml"));
        factory.setConfigLocation(new ClassPathResource(ehcacheLocation));
        //factory.setShared(true);
        return factory;
    }
     
    @Bean
    public CacheHelper cacheHelper() {
    	return new CacheHelper(); 
    }
    
    @SuppressWarnings({ "unchecked", "rawtypes" })
	@Bean
    public FilterRegistrationBean tuckeyRegistrationBean() {
        final FilterRegistrationBean registrationBean = new FilterRegistrationBean();
        registrationBean.setFilter(new CustomURLRewriter());
        return registrationBean;
    } 
    
}