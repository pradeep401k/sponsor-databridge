package com.slavic.webapp.config;

import java.util.Arrays;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import springfox.documentation.builders.ApiInfoBuilder;
import springfox.documentation.builders.PathSelectors;
import springfox.documentation.builders.RequestHandlerSelectors;
import springfox.documentation.service.ApiInfo;
import springfox.documentation.service.ApiKey;
import springfox.documentation.service.AuthorizationScope;
import springfox.documentation.service.Contact;
import springfox.documentation.service.SecurityReference;
import springfox.documentation.spi.DocumentationType;
import springfox.documentation.spi.service.contexts.SecurityContext;
import springfox.documentation.spring.web.plugins.Docket;
import springfox.documentation.swagger2.annotations.EnableSwagger2;

// @Configuration
@EnableSwagger2
public class SwaggerConfig {
   
   @Bean
   public Docket apiDocket() {
       
       Docket docket =  new Docket(DocumentationType.SWAGGER_2)
                .select()
                .apis(RequestHandlerSelectors.basePackage("com.slavic.web.controllers"))
                .paths(PathSelectors.any())
                .build()
                .securityContexts(Arrays.asList(securityContext()))
                .apiInfo(apiInfo())
                .securitySchemes(Arrays.asList(apiKey()));
       
       return docket;
       
    }
   
	private ApiInfo apiInfo() {
		return new ApiInfoBuilder().title("REST API").description("Bespoke API Documentation")
				.termsOfServiceUrl("").contact(new Contact("Slavic401k", "https://www.slavic401k.com", "systemslist@slavic401k.com"))
				.license("Apache License Version 2.0").licenseUrl("https://www.apache.org/licenses/LICENSE-2.0")
				.version("0.0.1").build();
	}

	private ApiKey apiKey() {
		return new ApiKey("authkey", "Authorization", "header");
	}
	
    private SecurityContext securityContext() {
        return SecurityContext.builder()
                .securityReferences(Arrays.asList(new SecurityReference("authkey", scopes())))
                .forPaths(PathSelectors.regex("/.*"))
                .build();
    }
    
    private AuthorizationScope[] scopes() {
        AuthorizationScope[] scopes = {
                new AuthorizationScope("read", "for read operations"),
                new AuthorizationScope("write", "for write operations") };
        return scopes;
    }
}
