package com.slavic.springboot.security.jwt;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.slavic.util.JsonResponseError;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.InternalAuthenticationServiceException;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.web.authentication.AbstractAuthenticationProcessingFilter;
import org.springframework.security.web.util.matcher.AntPathRequestMatcher;

import java.io.IOException;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

public class JWTLoginFilter extends AbstractAuthenticationProcessingFilter{

    private TokenAuthenticationService tokenAuthenticationService;
    
    private static final Logger log = LogManager.getLogger(JWTLoginFilter.class);
    
    @Value("${invalid.clientid.username}")
    private String invalidClientidorUserName;
    

    public JWTLoginFilter(String url, AuthenticationManager authenticationManager)
    {
        super(new AntPathRequestMatcher(url));
        setAuthenticationManager(authenticationManager);
        tokenAuthenticationService = new TokenAuthenticationService();
    }

    @Override
    public Authentication attemptAuthentication(HttpServletRequest httpServletRequest, HttpServletResponse httpServletResponse)
            throws AuthenticationException, IOException, ServletException {
    	try {
			String methodName=httpServletRequest.getMethod();    	
			log.debug("Attempt authentication...Method:"+methodName);
			if(!"POST".equals(methodName)) {
				JsonResponseError.setJsonResponseError(httpServletRequest, httpServletResponse, "Request method '"+methodName+"' not supported");
				return null;
			}
			
			AccountCredentials credentials = new ObjectMapper().readValue(httpServletRequest.getInputStream(),AccountCredentials.class);
			String userNameCliendId = credentials.getUsername();
			UsernamePasswordAuthenticationToken token = new UsernamePasswordAuthenticationToken(userNameCliendId, credentials.getPassword());                	
			return getAuthenticationManager().authenticate(token);
        } catch (InternalAuthenticationServiceException e) {
            log.debug("Requested User Id not found......"+ e.getMessage());
            JsonResponseError.setJsonResponseError(httpServletRequest, httpServletResponse, "user id not found in the system");
            return null;
        }
    }

    @Override
    protected void successfulAuthentication(HttpServletRequest request, HttpServletResponse response, FilterChain chain, Authentication authentication)
            throws IOException, ServletException{
    	String name = authentication.getName();    	
    	String peoId = request.getHeader("peoId");
    	if( peoId != null) {
        	log.debug("Successful Authentication happened for........" + name + ".....for peoId........" + peoId);
    	}else {
    		log.debug("Successful Authentication happened for........" + name + " with no peoId in header");
    	}
        tokenAuthenticationService.addAuthentication(response,name,peoId);
    }
    
    @Override
    protected void unsuccessfulAuthentication(HttpServletRequest request, HttpServletResponse response, AuthenticationException exception) throws IOException, ServletException{
    	log.debug("invalid clientid or credential..."+exception.getMessage());
    	JsonResponseError.setJsonResponseError(request, response, "Invalid header 'Client id' request or Bad credential");
    }    
}
