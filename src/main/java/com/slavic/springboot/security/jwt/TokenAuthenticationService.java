package com.slavic.springboot.security.jwt;

import io.jsonwebtoken.ExpiredJwtException;
import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.SignatureAlgorithm;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.security.core.Authentication;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.Date;


public class TokenAuthenticationService {

	private static final Logger logger = LogManager.getLogger(TokenAuthenticationService.class);
    private long EXPIRATIONTIME = 1000 * 4 * 60 * 60; // 4 hours
    private long EXPIRATIONTIME_REFRESH = 10*60*1000; //10 min 
    private String secret = "9agd45L1Oubt0WP2dy8S4Q==";
    private String tokenPrefix = "Bearer";
    private String headerString = "Authorization";   
   // private String header2String = "AuthToken";
    
    
    public void addAuthentication(HttpServletResponse response, String username, String peoId)
    {
        // We generate a token now.
    	String JWT = getAccessToken(username, peoId);
        response.addHeader(headerString,tokenPrefix + " "+ JWT);
      //  response.addHeader(header2String,tokenPrefix + " "+ JWT);
     //   response.setHeader("Access-Control-Expose-Headers", header2String);
        response.setHeader("Access-Control-Allow-Origin", "*");
    }

    public Authentication getAuthentication(HttpServletRequest request, HttpServletResponse response)
    {
        String token = request.getHeader(headerString);
        String url = request.getRequestURL().toString();
        String username = null;
        if(token != null)
        {
        	try {
            // parse the token.
             username = parseToken(token);
            if(username != null) // we managed to retrieve a user
            {
                return new AuthenticatedUser(username);
            }
        	}catch (ExpiredJwtException e) {
         	
         		if(url.indexOf("token-refresh") > 0) {
	         		if( (new Date().getTime() -  e.getClaims().getExpiration().getTime()<=EXPIRATIONTIME_REFRESH)){
	         			logger.info("time to refresh token");
	         		String JWT =	this.getAccessToken(e.getClaims().getSubject(), e.getClaims().getIssuer(), e.getClaims().getAudience());
	         		 response.addHeader(headerString,tokenPrefix + " "+ JWT);
	               //  response.addHeader(header2String,tokenPrefix + " "+ JWT);
	               
	                 try {
						username =parseToken(JWT);
	                 } catch (Exception e1) {
						logger.error("getAuthentication issue: "+e1.getMessage());
					}
	                 return new AuthenticatedUser(username);
	         		}else {
	         			logger.info("time to refresh token has expired");
	         		}
         		}
         }catch (Exception e) {
        		logger.error("getAuthentication issue: "+e.getMessage());
        		return null;
        	}
        }
        return null;
    }
    

	public String getAccessToken(String participantId, String planId,String tpaId) {
		String JWT = Jwts.builder().setSubject(participantId).setIssuer(planId).setAudience(tpaId)
				.setExpiration(new Date(System.currentTimeMillis() + EXPIRATIONTIME))
				.signWith(SignatureAlgorithm.HS512, secret).compact();
		return JWT;
	}
		
    
    public String getAccessToken(String username, String peoId) {
   	 String JWT = Jwts.builder()
                .setSubject(username)
                .setIssuer(peoId)
                .setExpiration(new Date(System.currentTimeMillis() + EXPIRATIONTIME))
                .signWith(SignatureAlgorithm.HS512, secret)
                .compact();
   	 
   	 return JWT;
   }
	
	public String parseToken(String toke) throws  Exception{
	    	
	    	String username = Jwts.parser()
	                .setSigningKey(secret)
	                .parseClaimsJws(toke)
	                .getBody()
	                .getSubject();
	    	
	    	return username;
	    	
	}

	public String getIssuer(String token) {
		String id = Jwts.parser().setSigningKey(secret).parseClaimsJws(token).getBody().getIssuer();
		return id;
	}

	public String getSubject(String token) {
		// parse the token.
		String username = Jwts.parser().setSigningKey(secret).parseClaimsJws(token).getBody().getSubject();
		return username;
	}
	
	public String getAudience(String token) {
		// parse the token.
		String audience = Jwts.parser().setSigningKey(secret).parseClaimsJws(token).getBody().getAudience();
		return audience;
	}
    
}
