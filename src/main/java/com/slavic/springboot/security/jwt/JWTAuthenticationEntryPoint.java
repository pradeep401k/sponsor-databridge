package com.slavic.springboot.security.jwt;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.slavic.springboot.constant.Constants;

import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.web.AuthenticationEntryPoint;
import org.springframework.stereotype.Component;
 
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.OutputStream;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import static javax.servlet.http.HttpServletResponse.SC_FORBIDDEN;
 
@Component
public class JWTAuthenticationEntryPoint implements AuthenticationEntryPoint
{
	 @Value("${jwt.exception.1000}")
	 private String expiredToken;
	
    @Override
    public void commence(HttpServletRequest httpServletRequest, HttpServletResponse httpServletResponse, AuthenticationException e)
            throws IOException, ServletException {
        httpServletResponse.setStatus(SC_FORBIDDEN);
        httpServletResponse.setContentType(MediaType.APPLICATION_JSON_VALUE); 
        String errorCode = "";
        String errorMessage = "";
        if(httpServletRequest.getAttribute("errorCode") != null) {
        	errorCode = (String) httpServletRequest.getAttribute(Constants.ERRORCODE);
        	errorMessage = (String)httpServletRequest.getAttribute("errorMessage");
        }
        Map<String, Object> data = new HashMap<>();
        data.put("timestamp", new Date());
        data.put("status",HttpStatus.FORBIDDEN.value());
        //data.put("errorMessage", e.getMessage());
        data.put("path", httpServletRequest.getRequestURL().toString());
        if(StringUtils.isNotBlank(errorCode)) {
        	data.put(Constants.ERRORMESSAGE, expiredToken);
        	data.put(Constants.ERRORCODE, errorCode);
        }else {
        	data.put(Constants.ERRORMESSAGE, e.getMessage());
        	data.put(Constants.ERRORCODE, "100");
        }
        
        OutputStream out = httpServletResponse.getOutputStream();
        com.fasterxml.jackson.databind.ObjectMapper mapper = new ObjectMapper();
        mapper.writeValue(out, data);
        out.flush();
    }
}
