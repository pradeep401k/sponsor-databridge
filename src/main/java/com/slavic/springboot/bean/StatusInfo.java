package com.slavic.springboot.bean;

import java.io.Serializable;
import java.time.LocalDate;

import javax.persistence.Transient;

import com.fasterxml.jackson.annotation.JsonInclude;


public class StatusInfo implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	@JsonInclude(JsonInclude.Include.NON_NULL)
	@Transient
	private String status;
	
	@JsonInclude(JsonInclude.Include.NON_NULL)
	@Transient
	private String statusCode;
	
	@JsonInclude(JsonInclude.Include.NON_NULL)
	@Transient
	private String returnUrl;
	
	@JsonInclude(JsonInclude.Include.NON_DEFAULT)
	@Transient
	private int enrollStage;
	
	@JsonInclude(JsonInclude.Include.NON_NULL)
	@Transient
	private LocalDate enrolledDate;
	
	public StatusInfo() {		
	}
	
	public StatusInfo(String status) {
		this.status=status;
	}
	
	public StatusInfo(String status, String statusCode) {
		this.status=status;
		this.statusCode = statusCode;
	}
	

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public String getStatusCode() {
		return statusCode;
	}

	public void setStatusCode(String statusCode) {
		this.statusCode = statusCode;
	}

	public String getReturnUrl() {
		return returnUrl;
	}

	public void setReturnUrl(String returnUrl) {
		this.returnUrl = returnUrl;
	}

	public int getEnrollStage() {
		return enrollStage;
	}

	public void setEnrollStage(int enrollStage) {
		this.enrollStage = enrollStage;
	}

	public LocalDate getEnrolledDate() {
		return enrolledDate;
	}

	public void setEnrolledDate(LocalDate enrolledDate) {
		this.enrolledDate = enrolledDate;
	}	

}
