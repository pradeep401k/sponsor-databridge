package com.slavic.springboot.bean;

import java.util.Collection;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.AuthorityUtils;


public class MyUser {

	private String username;
	private String password;
	private boolean enabled;
	

	public MyUser() {
	}

	public MyUser(String username, String password, boolean enabled) {
		this.username = username;
		this.password = password;
		this.enabled = enabled;
	}

	
	public String getUsername() {
		return this.username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	
	public String getPassword() {
		return this.password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	
	public boolean isEnabled() {
		return this.enabled;
	}

	public void setEnabled(boolean enabled) {
		this.enabled = enabled;
	}

	  public Collection<GrantedAuthority> getAuthorities() {
		    return AuthorityUtils.commaSeparatedStringToAuthorityList("ADMIN");
	 }
	  
	  public String getUserRole() {
			return "ROLE_USER";
		}
}
