package com.slavic.springboot.mysql.model;

import java.io.Serializable;
import java.math.BigDecimal;
import java.time.LocalDate;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.IdClass;
import javax.persistence.Table;

import org.apache.logging.log4j.core.config.plugins.validation.constraints.Required;
import org.hibernate.validator.constraints.NotBlank;

import com.fasterxml.jackson.annotation.JsonFormat;

@Entity
@Table(name="sponsor_portal.sponsor_overview_assets_total")
@IdClass(AssetsTotalKey.class)
public class AssetsTotal implements Serializable{


	private static final long serialVersionUID = 1L;
	
	@Id
	@Required
	@NotBlank(message="peoId is mandatory")
	@Column(name="PEO_ID")
	private String peoId;
	
	@Id
	@JsonFormat(pattern = "yyyy-MM-dd")
	@Column(name="DATE")
	private LocalDate date;

	
	@Column(name="TOTAL")
	private BigDecimal total;

	
	public AssetsTotal() {
		this.total= BigDecimal.ZERO;
		this.peoId = "NONE";
		this.date = null;
	}
	
	

	public AssetsTotal(String peoId, LocalDate date, BigDecimal total) {
		super();
		this.peoId = peoId;
		this.date = date;
		this.total = total;
	}



	public String getPeoId() {
		return peoId;
	}


	public void setPeoId(String peoId) {
		this.peoId = peoId;
	}


	public LocalDate getDate() {
		return date;
	}


	public void setDate(LocalDate date) {
		this.date = date;
	}


	public BigDecimal getTotal() {
		return total;
	}


	public void setTotal(BigDecimal total) {
		this.total = total;
	}
	
}
