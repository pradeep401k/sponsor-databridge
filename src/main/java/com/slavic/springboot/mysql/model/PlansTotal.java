package com.slavic.springboot.mysql.model;

import java.io.Serializable;
import java.math.BigDecimal;
import java.time.LocalDate;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.IdClass;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonFormat;

@Entity
@Table(name="sponsor_portal.sponsor_overview_plans_total")
@IdClass(PlansTotalKey.class)
public class PlansTotal implements Serializable{

	private static final long serialVersionUID = 1L;
	
	@Id
	@Column(name="PEO_ID")
	private String peoId;
	
	@Id
	@JsonFormat(pattern = "yyyy-MM-dd")
	@Column(name="DATE")
	private LocalDate date;
	
	@Column(name="TOTAL")
	private int total;
	
	@Column(name="SH_PLANS")
	private int safeHarborPlans;
	
	@Column(name="ASSETS_PLANS")
	private int assetsPlans;
	
	@Column(name="ACTIVE_PLANS")
	private int activePlans;
	
	@Column(name="CONTRIB_PLANS")
	private int contribPlans;
	
	
	public PlansTotal() {
		super();
		this.peoId="NONE";
		this.total=0;
		this.safeHarborPlans= 0 ;
		this.assetsPlans = 0;
		this.activePlans = 0;
		this.contribPlans= 0;
	}

	public String getPeoId() {
		return peoId;
	}

	public void setPeoId(String peoId) {
		this.peoId = peoId;
	}

	public LocalDate getDate() {
		return date;
	}

	public void setDate(LocalDate date) {
		this.date = date;
	}

	public int getTotal() {
		return total;
	}

	public void setTotal(int total) {
		this.total = total;
	}

	public int getSafeHarborPlans() {
		return safeHarborPlans;
	}

	public void setSafeHarborPlasn(int safeHarborPlans) {
		this.safeHarborPlans = safeHarborPlans;
	}

	public int getAssetsPlans() {
		return assetsPlans;
	}

	public void setAssetsPlans(int assetsPlans) {
		this.assetsPlans = assetsPlans;
	}

	public int getActivePlans() {
		return activePlans;
	}

	public void setActivePlans(int activePlans) {
		this.activePlans = activePlans;
	}

	public int getContribPlans() {
		return contribPlans;
	}

	public void setContribPlans(int contribPlans) {
		this.contribPlans = contribPlans;
	}

	public void setSafeHarborPlans(int safeHarborPlans) {
		this.safeHarborPlans = safeHarborPlans;
	}
		
}
