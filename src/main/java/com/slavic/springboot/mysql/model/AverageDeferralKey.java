package com.slavic.springboot.mysql.model;

import java.io.Serializable;
import java.time.LocalDate;

public class AverageDeferralKey implements Serializable{


	private static final long serialVersionUID = 1L;

	private String peoId;
	
	private LocalDate startDate;
	
	private LocalDate endDate;

	public String getPeoId() {
		return peoId;
	}

	public void setPeoId(String peoId) {
		this.peoId = peoId;
	}

	public LocalDate getStartDate() {
		return startDate;
	}

	public void setStartDate(LocalDate startDate) {
		this.startDate = startDate;
	}

	public LocalDate getEndDate() {
		return endDate;
	}

	public void setEndDate(LocalDate endDate) {
		this.endDate = endDate;
	}

	
	
	
	
}
