package com.slavic.springboot.mysql.model;

import java.io.Serializable;
import java.time.LocalDate;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.IdClass;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonFormat;

@Entity
@Table(name="sponsor_portal.sponsor_overview_plans_changes")
@IdClass(PlansChangesKey.class)
public class PlansChanges implements Serializable {

	
	private static final long serialVersionUID = 1L;

	@Id
	@Column(name="PEO_ID")
	private String peoId;
	
	@Id
	@JsonFormat(pattern = "yyyy-MM-dd")
	@Column(name="START_DATE")
	private LocalDate start;
	
	@Id
	@JsonFormat(pattern = "yyyy-MM-dd")
	@Column(name = "END_DATE")
	private LocalDate end;
	
	@Column(name = "NEW_PLANS")
	private int newPlans;
	
	@Column(name="MERGE_IN")
	private int mergeIn;
	
	@Column(name="MERGE_OUT")
	private int mergeOut;

	public String getPeoId() {
		return peoId;
	}

	public void setPeoId(String peoId) {
		this.peoId = peoId;
	}

	public LocalDate getStart() {
		return start;
	}

	public void setStart(LocalDate start) {
		this.start = start;
	}

	public LocalDate getEnd() {
		return end;
	}

	public void setEnd(LocalDate end) {
		this.end = end;
	}

	public int getNewPlans() {
		return newPlans;
	}

	public void setNewPlans(int newPlans) {
		this.newPlans = newPlans;
	}

	public int getMergeIn() {
		return mergeIn;
	}

	public void setMergeIn(int mergeIn) {
		this.mergeIn = mergeIn;
	}

	public int getMergeOut() {
		return mergeOut;
	}

	public void setMergeOut(int mergeOut) {
		this.mergeOut = mergeOut;
	}
	
	
}
