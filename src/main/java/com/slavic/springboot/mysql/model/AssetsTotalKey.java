package com.slavic.springboot.mysql.model;

import java.io.Serializable;
import java.time.LocalDate;

import javax.persistence.Convert;


public class AssetsTotalKey implements Serializable {

	private static final long serialVersionUID = 1L;

	private String peoId;
	
	private LocalDate date;

	public String getPeoId() {
		return peoId;
	}

	public void setPeoId(String peoId) {
		this.peoId = peoId;
	}

	public LocalDate getDate() {
		return date;
	}

	public void setDate(LocalDate date) {
		this.date = date;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((date == null) ? 0 : date.hashCode());
		result = prime * result + ((peoId == null) ? 0 : peoId.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		AssetsTotalKey other = (AssetsTotalKey) obj;
		if (date == null) {
			if (other.date != null)
				return false;
		} else if (!date.equals(other.date))
			return false;
		if (peoId == null) {
			if (other.peoId != null)
				return false;
		} else if (!peoId.equals(other.peoId))
			return false;
		return true;
	}
	
}
