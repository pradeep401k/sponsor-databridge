package com.slavic.springboot.mysql.model;

import java.io.Serializable;
import java.time.LocalDate;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity(name="ParticipantChangesEntity")
@Table(name="sponsor_portal.sponsor_overview_participants_changes")
public class ParticipantChangesEntity implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name="id")
	private Long Id;
	@Column(name="peo_id")
	private String peoId;
	@Column(name="DATE")
	private LocalDate asOfDate;
	@Column(name="total_participants")
	private int totalParticipants;
	@Column(name="new_eligible_participants")
	private int newEligibleParticipants;
	@Column(name="new_enrollments")
	private int newEnrollments;
	@Column(name="deferring_participants")
	private int contributing;
	@Column(name="eligible_participants")
	private int eligible;
	@Column(name="participants_with_assets")
	private int withAssets;
	
	
	public ParticipantChangesEntity() {
		super();
		this.peoId = "NONE";
		this.totalParticipants = 0;
		this.newEligibleParticipants = 0;
		this.newEnrollments = 0;
		this.contributing=0;
		this.eligible =0;
		this.withAssets=0;
	}


	public Long getId() {
		return Id;
	}
	public void setId(Long id) {
		Id = id;
	}
	public String getPeoId() {
		return peoId;
	}
	public void setPeoId(String peoId) {
		this.peoId = peoId;
	}
	public LocalDate getAsOfDate() {
		return asOfDate;
	}
	public void setAsOfDate(LocalDate asOfDate) {
		this.asOfDate = asOfDate;
	}


	public int getTotalParticipants() {
		return totalParticipants;
	}


	public void setTotalParticipants(int totalParticipants) {
		this.totalParticipants = totalParticipants;
	}


	public int getNewEligibleParticipants() {
		return newEligibleParticipants;
	}


	public void setNewEligibleParticipants(int newEligibleParticipants) {
		this.newEligibleParticipants = newEligibleParticipants;
	}


	public int getNewEnrollments() {
		return newEnrollments;
	}


	public void setNewEnrollments(int newEnrollments) {
		this.newEnrollments = newEnrollments;
	}


	public int getContributing() {
		return contributing;
	}


	public void setContributing(int contributing) {
		this.contributing = contributing;
	}


	public int getEligible() {
		return eligible;
	}


	public void setEligible(int eligible) {
		this.eligible = eligible;
	}


	public int getWithAssets() {
		return withAssets;
	}


	public void setWithAssets(int withAssets) {
		this.withAssets = withAssets;
	}
	
	
	

}
