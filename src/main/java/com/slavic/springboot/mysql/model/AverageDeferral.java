package com.slavic.springboot.mysql.model;

import java.io.Serializable;
import java.time.LocalDate;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.IdClass;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonFormat;

@Entity
@Table(name ="sponsor_portal.sponsor_overview_avg_deferral")
@IdClass(AverageDeferralKey.class)
public class AverageDeferral implements Serializable{

	private static final long serialVersionUID = 1L;

	@Id
	@Column(name="PEO_ID")
	private String peoId;
	
	@Id
	@JsonFormat(pattern = "yyyy-MM-dd")
	@Column(name="start_date")
	private LocalDate startDate;
	
	@Id
	@JsonFormat(pattern = "yyyy-MM-dd")
	@Column(name="end_date")
	private LocalDate endDate;
	
	@Column(name="AVERAGE")
	private double average;


	public AverageDeferral() {
		super();
		this.average= 0;
		this.peoId="NONE";
	}
	
	

	



	public AverageDeferral(String peoId, LocalDate startDate, LocalDate endDate, double average) {
		super();
		this.peoId = peoId;
		this.startDate = startDate;
		this.endDate = endDate;
		this.average = average;
	}


	public String getPeoId() {
		return peoId;
	}

	public void setPeoId(String peoId) {
		this.peoId = peoId;
	}

	public LocalDate getStartDate() {
		return startDate;
	}

	public void setStartDate(LocalDate startDate) {
		this.startDate = startDate;
	}

	public LocalDate getEndDate() {
		return endDate;
	}

	public void setEndDate(LocalDate endDate) {
		this.endDate = endDate;
	}

	public double getAverage() {
		return average;
	}

	public void setAverage(double average) {
		this.average = average;
	}
	
}
