package com.slavic.springboot.mysql.model;

import java.io.Serializable;
import java.math.BigDecimal;
import java.time.LocalDate;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.IdClass;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonFormat;

@Entity
@Table(name ="sponsor_portal.sponsor_overview_assets_changes")
@IdClass(AssetsChangesKey.class)
public class AssetsChanges implements Serializable {

	private static final long serialVersionUID = 1L;

	@Id
	@Column(name ="peo_id")
	private String peoId;
	
	@Id
	@JsonFormat(pattern = "yyyy-MM-dd")
	@Column(name ="START_DATE")
	private LocalDate start;
	
	@Id
	@JsonFormat(pattern = "yyyy-MM-dd")
	@Column(name ="END_DATE")
	private LocalDate end;
	
	@Column(name ="CONTRIBUTIONS")
	private BigDecimal contributions;
	
	@Column(name ="DISTRIBUTIONS")
	private BigDecimal distributions;
	
	@Column(name ="TRANSFER_IN")
	private BigDecimal transferIn;
	
	@Column(name ="TRANSFER_OUT")	
	private BigDecimal TransferOut;

	
	public AssetsChanges() {
		this.peoId="NONE";
		this.contributions= BigDecimal.ZERO;
		this.distributions=BigDecimal.ZERO;;
		this.transferIn = BigDecimal.ZERO;
		this.TransferOut = BigDecimal.ZERO;
	}
	
	
	
	public AssetsChanges(String peoId, LocalDate start, LocalDate end, BigDecimal contributions,
			BigDecimal distributions, BigDecimal transferIn, BigDecimal transferOut) {
		super();
		this.peoId = peoId;
		this.start = start;
		this.end = end;
		this.contributions = contributions;
		this.distributions = distributions;
		this.transferIn = transferIn;
		TransferOut = transferOut;
	}



	public String getPeoId() {
		return peoId;
	}

	public void setPeoId(String peoId) {
		this.peoId = peoId;
	}

	public LocalDate getStart() {
		return start;
	}

	public void setStart(LocalDate start) {
		this.start = start;
	}

	public LocalDate getEnd() {
		return end;
	}

	public void setEnd(LocalDate end) {
		this.end = end;
	}

	public BigDecimal getContributions() {
		return contributions;
	}

	public void setContributions(BigDecimal contributions) {
		this.contributions = contributions;
	}

	public BigDecimal getDistributions() {
		return distributions;
	}

	public void setDistributions(BigDecimal distributions) {
		this.distributions = distributions;
	}

	public BigDecimal getTransferIn() {
		return transferIn;
	}

	public void setTransferIn(BigDecimal transferIn) {
		this.transferIn = transferIn;
	}

	public BigDecimal getTransferOut() {
		return TransferOut;
	}

	public void setTransferOut(BigDecimal transferOut) {
		TransferOut = transferOut;
	}
	
	
}
