package com.slavic.springboot.mysql.repository;

import java.time.LocalDate;
import java.util.List;
import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import com.slavic.springboot.mysql.model.ParticipantChangesEntity;


public interface ParticipantsChangesRepository extends JpaRepository<ParticipantChangesEntity, Long>{
	
	@Query("select p from ParticipantChangesEntity p where p.peoId =:peoId and p.asOfDate = :paramDate")
	Optional<ParticipantChangesEntity> getInfoByPeoIdAndDate(@Param("peoId")String peoId,@Param("paramDate")LocalDate paramDate);

	@Query(value = "select id, peo_id, DATE, total_participants, new_eligible_participants, new_enrollments, deferring_participants,\r\n" + 
			"eligible_participants, participants_with_assets\r\n" + 
			"from sponsor_portal.sponsor_overview_participants_changes\r\n" + 
			"where peo_id = ?1 and date >= ?2 and date <= ?3 \r\n"+
			"and (DATE = last_day(DATE) || month(DATE) = month(current_date()))\r\n"+
			"group by year(date), month(date) order by date desc limit ?4 ", nativeQuery = true)
	List<ParticipantChangesEntity> getInfoByPeoIdAndMonth(String peoId,LocalDate startDate,LocalDate endDate, int limit);
	
	@Query(value = "select id, peo_id, DATE, total_participants, sum(new_eligible_participants) as new_eligible_participants, sum(new_enrollments) as new_enrollments, \r\n" + 
			"		deferring_participants,eligible_participants, participants_with_assets from (\r\n" + 
			"select *\r\n" + 
			"	from sponsor_portal.sponsor_overview_participants_changes\r\n" + 
			"	where peo_id = ?1 and date between ?2 and  ?3 \r\n" + 
			"	and (DATE = last_day(DATE) || month(DATE) = month(current_date()))\r\n" + 
			"	group by year(date),  month(date) order by date desc \r\n" + 
			") q1 group by year(q1.date), quarter(q1.date) order by q1.date desc limit ?4", nativeQuery = true)
	List<ParticipantChangesEntity> getInfoByPeoIdAndQuarter(String peoId,LocalDate startDate,LocalDate endDate, int limit);
	
	@Query(value = "select id, peo_id, DATE, total_participants, sum(new_eligible_participants) as new_eligible_participants, sum(new_enrollments) as new_enrollments, \r\n" + 
			"		deferring_participants,eligible_participants, participants_with_assets from (\r\n" + 
			"select *\r\n" + 
			"	from sponsor_portal.sponsor_overview_participants_changes\r\n" + 
			"	where peo_id = ?1 and date between ?2 and  ?3 \r\n" + 
			"	and (DATE = last_day(DATE) || month(DATE) = month(current_date()))\r\n" + 
			"	group by year(date),  month(date) order by date desc \r\n" + 
			") q1 group by year(q1.date) order by q1.date desc limit ?4", nativeQuery = true)
	List<ParticipantChangesEntity> getInfoByPeoIdAndYear(String peoId,LocalDate startDate,LocalDate endDate, int limit);
}
