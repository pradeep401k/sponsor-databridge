package com.slavic.springboot.mysql.repository;

import java.time.LocalDate;
import java.util.List;
import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import com.slavic.springboot.mysql.model.AverageDeferral;


@Repository
public interface AverageDeferralRepository extends JpaRepository<AverageDeferral, Long> {

	public Optional<AverageDeferral> findByPeoIdAndStartDateAndEndDate(String peoId, LocalDate startDate, LocalDate endDate);
	
	@Query(value="SELECT * FROM sponsor_portal.sponsor_overview_avg_deferral where  peo_id=?1  and start_date >=?2 and end_date <=?3 order by end_date desc",  nativeQuery=true)
	public List<AverageDeferral> findByDateRange(String peoId, LocalDate start, LocalDate end);
	
	
	@Query(value="select peo_id, start_date, end_date, AVERAGE from (\r\n" + 
			"	select  max(start_date) as sd, max(end_date) as ed\r\n" + 
			"	from  sponsor_portal.sponsor_overview_avg_deferral \r\n" + 
			"	where  peo_id = ?1 and start_date >= ?2 and end_date <= ?3 \r\n" + 
			"	and (end_date = last_day(end_date) || month(end_date) = month(current_date()))\r\n" + 
			"	and month(start_date) = month(end_date) \r\n" + 
			"	group by month(start_date), year(start_date) \r\n" + 
			") q1\r\n" + 
			"inner join \r\n" + 
			"(select peo_id, start_date, end_date, AVERAGE\r\n" + 
			"	from  sponsor_portal.sponsor_overview_avg_deferral \r\n" + 
			"	where  peo_id = ?1 and start_date >= ?2 and end_date <= ?3 \r\n" + 
			"	and (end_date = last_day(end_date) || month(end_date) = month(current_date()))\r\n" + 
			"    and month(start_date) = month(end_date)  \r\n" + 
			"    ) q2 on q1.sd = q2.start_date and q1.ed = q2.end_date order by end_date desc limit ?4",  nativeQuery=true)
	public List<AverageDeferral> findByMonthRange(String peoId, LocalDate start, LocalDate end, int limit);
	
	@Query(value="SELECT peo_id, start_date, end_date, AVERAGE\r\n" + 
			"FROM sponsor_portal.sponsor_overview_avg_deferral where peo_id=?1 and start_date >=?2 and end_date <= ?3\r\n" + 
			"and start_date = MAKEDATE(year(start_date),1)\r\n" + 
			"and (end_date = concat(year(end_date),'-12-31')\r\n" + 
			"      or end_date=(SELECT max(end_date)\r\n" + 
			"      FROM sponsor_portal.sponsor_overview_avg_deferral where peo_id=?1  and end_date <= ?3\r\n" + 
			"      and start_date = MAKEDATE(year(end_date),1)))\r\n" + 
			"order by end_date desc limit ?4 ",  nativeQuery=true)
	public List<AverageDeferral> findByYearRange(String peoId, LocalDate start, LocalDate end, int limit);
	
	@Query(value="SELECT peo_id, start_date, end_date, AVERAGE\r\n" + 
			"FROM sponsor_portal.sponsor_overview_avg_deferral where peo_id=?1 and start_date >=?2 and end_date<=?3\r\n" + 
			"and start_date = (MAKEDATE(Year(start_date),1)+ INTERVAL  QUARTER(start_date) QUARTER - INTERVAL  1 QUARTER )\r\n" + 
			"and  (end_date = (MAKEDATE(Year(end_date),1)+ INTERVAL  QUARTER(end_date) QUARTER - INTERVAL  1 DAY ) ||\r\n" + 
			"(end_date = (SELECT max(end_date) FROM sponsor_portal.sponsor_overview_avg_deferral where peo_id=?1   and end_date<=?3\r\n" + 
			"and  start_date = (MAKEDATE(Year(start_date),1)+ INTERVAL  QUARTER(start_date) QUARTER - INTERVAL  1 QUARTER ))))\r\n" + 
			"and quarter(start_date) = quarter(end_date)\n" + 
			"order by end_date desc limit ?4",  nativeQuery=true)
	public List<AverageDeferral> findByQuarterRange(String peoId, LocalDate start, LocalDate end, int limit); 
}
