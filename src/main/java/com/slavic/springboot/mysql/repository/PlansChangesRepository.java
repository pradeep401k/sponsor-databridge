package com.slavic.springboot.mysql.repository;

import java.time.LocalDate;
import java.util.List;
import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import com.slavic.springboot.mysql.model.PlansChanges;


@Repository
public interface PlansChangesRepository extends JpaRepository<PlansChanges, Long> {

	
	public Optional<PlansChanges> findByPeoIdAndStartAndEnd(String peoId, LocalDate start, LocalDate end);
	
	
	@Query(value ="SELECT *  FROM sponsor_portal.sponsor_overview_plans_changes where PEO_ID=?1 and START_DATE>=?2 and END_DATE<=?3 order by start_date desc", nativeQuery=true)
	public List<PlansChanges> findByRange(String peoId, LocalDate start, LocalDate end);
		
	@Query(value ="select distinct PEO_ID, min(START_DATE) as START_DATE, max(END_DATE) as END_DATE, sum(NEW_PLANS) as 'NEW_PLANS', sum(merge_in) as 'MERGE_IN', sum(MERGE_OUT) as 'MERGE_OUT' \r\n" + 
			"from sponsor_portal.sponsor_overview_plans_changes a \r\n" + 
			"where peo_id = ?1 \r\n" + 
			"and START_DATE >= ?2 and END_DATE <= ?3 \r\n" + 
			"and (a.END_DATE = last_day(start_date) || month(end_date) = month(current_date()))\r\n" + 
			"and a.start_date = DATE_ADD(LAST_DAY((end_date - INTERVAL 1 MONTH)), interval 1 day)\r\n" + 
			"group by year(end_date), month(end_date) order by start_date desc limit ?4 ", nativeQuery=true)
	public List<PlansChanges> findByMonthRange(String peoId, LocalDate start, LocalDate end, int limit);
	
	@Query(value ="select distinct PEO_ID, START_DATE, max(END_DATE) as END_DATE, sum(NEW_PLANS) as 'NEW_PLANS', sum(merge_in) as 'MERGE_IN', sum(MERGE_OUT) as 'MERGE_OUT' \r\n" + 
			"from sponsor_portal.sponsor_overview_plans_changes a where peo_id = ?1 \r\n" + 
			"and START_DATE >= ?2 and END_DATE <= ?3 and (a.END_DATE = last_day(start_date) || month(end_date) = month(current_date())) \r\n" + 
			"and a.start_date = DATE_ADD(LAST_DAY((end_date - INTERVAL 1 MONTH)), interval 1 day)\r\n" + 
			"group by year(end_date) order by start_date desc limit ?4 ", nativeQuery=true)
	public List<PlansChanges> findByYearRange(String peoId, LocalDate start, LocalDate end, int limit); 
	
	@Query(value ="select distinct PEO_ID, min(START_DATE) as START_DATE, max(END_DATE) as END_DATE, sum(NEW_PLANS) as 'NEW_PLANS', sum(merge_in) as 'MERGE_IN', sum(MERGE_OUT) as 'MERGE_OUT' \r\n" + 
			"from sponsor_portal.sponsor_overview_plans_changes a where peo_id = ?1 \r\n" + 
			"and START_DATE >= ?2 and END_DATE <= ?3 and (a.END_DATE = last_day(start_date) || month(end_date) = month(current_date())) \r\n" + 
			"and a.start_date = DATE_ADD(LAST_DAY((end_date - INTERVAL 1 MONTH)), interval 1 day)\r\n" + 
			"group by year(end_date), quarter(end_date) order by start_date desc limit ?4", nativeQuery=true)
	public List<PlansChanges> findByQuarterRange(String peoId, LocalDate start, LocalDate end, int limit); 
	
	
	
	
}
