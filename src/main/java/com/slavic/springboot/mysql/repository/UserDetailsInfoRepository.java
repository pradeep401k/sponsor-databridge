package com.slavic.springboot.mysql.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import com.slavic.springboot.mysql.model.UserDetailsInfo;

public interface UserDetailsInfoRepository extends JpaRepository<UserDetailsInfo, Long>{
	
	@Query("select new UserDetailsInfo(u.userid, u.password, u.adminid) from UserDetailsInfo u where u.userid is not null")
	List<UserDetailsInfo> getAllUserInfo();

}
