package com.slavic.springboot.mysql.repository;

import java.time.LocalDate;
import java.util.List;
import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import com.slavic.springboot.mysql.model.AssetsChanges;

@Repository
public interface AssetsChangesRepository extends JpaRepository<AssetsChanges, Long> {

	public Optional<AssetsChanges> findByPeoIdAndStartAndEnd(String peoId, LocalDate start, LocalDate end);
	
	
	@Query(value ="SELECT *  FROM sponsor_portal.sponsor_overview_assets_changes where peo_id=?1 and START_DATE>=?2 and END_DATE<=?3 order by start_date desc", nativeQuery=true)
	public List<AssetsChanges> findByRange(String peoId, LocalDate start, LocalDate end);
	
	@Query(value ="select distinct peo_id, min(start_date) as START_DATE, max(end_date) as END_DATE, sum(contributions) as CONTRIBUTIONS, sum(distributions) as DISTRIBUTIONS, sum(transfer_in) as TRANSFER_IN, sum(transfer_out) as TRANSFER_OUT \r\n" + 
			"from sponsor_portal.sponsor_overview_assets_changes a where peo_id = ?1 \r\n" + 
			"and start_date >= ?2 and end_date <= ?3 and a.end_date = last_day(start_date) \r\n" + 
			"and a.start_date = DATE_ADD(LAST_DAY((end_date - INTERVAL 1 MONTH)), interval 1 day)\r\n" + 
			"group by year(end_date) order by start_date desc limit ?4", nativeQuery=true)
	public List<AssetsChanges> findAssetChangesPerYear(String peoId, LocalDate start, LocalDate end, int limit);
	
	
	@Query(value ="select distinct peo_id, min(start_date) as START_DATE, max(end_date) as END_DATE, sum(contributions) as CONTRIBUTIONS, sum(distributions) as DISTRIBUTIONS, sum(transfer_in) as TRANSFER_IN, sum(transfer_out) as TRANSFER_OUT \r\n" + 
			"from sponsor_portal.sponsor_overview_assets_changes a where peo_id = ?1 \r\n" + 
			"and start_date >= ?2 and end_date <= ?3 and a.end_date = last_day(start_date)  \r\n" + 
			"and a.start_date = DATE_ADD(LAST_DAY((end_date - INTERVAL 1 MONTH)), interval 1 day)\r\n" + 
			"group by year(end_date), quarter(end_date) order by start_date desc limit ?4", nativeQuery=true)
	public List<AssetsChanges> findAssetsChangesByQuarter(String peoId, LocalDate start, LocalDate end, int limit);
}
