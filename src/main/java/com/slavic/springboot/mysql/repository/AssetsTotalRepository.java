package com.slavic.springboot.mysql.repository;

import java.time.LocalDate;
import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import com.slavic.springboot.mysql.model.AssetsTotal;

@Repository
public interface AssetsTotalRepository extends JpaRepository<AssetsTotal, Long> {

public Optional<AssetsTotal> findByPeoIdAndDate(String peoId, LocalDate date);
	
	public Optional<AssetsTotal> findByPeoId(String peoId);
	
	@Query(value ="Select * From sponsor_portal.sponsor_overview_assets_total where PEO_ID =?1 and DATE = ?2", nativeQuery=true)
	public Optional<AssetsTotal> findAssetsByYear(String peoId, LocalDate start);
	
	
}
