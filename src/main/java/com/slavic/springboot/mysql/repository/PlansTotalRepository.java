package com.slavic.springboot.mysql.repository;

import java.time.LocalDate;
import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.slavic.springboot.mysql.model.PlansTotal;

@Repository
public interface PlansTotalRepository extends JpaRepository<PlansTotal, Long> {

	public Optional<PlansTotal> findByPeoIdAndDate(String peoId, LocalDate date);
	
	public Optional<PlansTotal> findByPeoId(String peoId);
	
}
