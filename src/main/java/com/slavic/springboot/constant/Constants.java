package com.slavic.springboot.constant;

public final class Constants {	
	public static String ERRORMESSAGE="errorMessage";
	public static String ERRORCODE="errorCode";
	public static String EXPIREDTOKENCODE="1000";
	public static String TIMESTAMP="timestamp";
	public static String STATUS="status";
	public static String PATH="path";	
	public static String SUCCESS= "success";
	public static String FAILURE= "failure";
	public static String RECORDNOTFOUND = "No Records are there";
	public static String AUTH="Authorization";
	public static String SUCCESSCODE="3000";
	public static String FAILURECODE = "2005";
	
	public static int BADREQUESTCODE=400;
}
