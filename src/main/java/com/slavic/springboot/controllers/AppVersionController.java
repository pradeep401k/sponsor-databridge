package com.slavic.springboot.controllers;

import java.io.InputStream;
import java.net.InetAddress;
import java.time.LocalTime;
import java.time.ZoneId;
import java.util.HashMap;
import java.util.Map;
import java.util.jar.Manifest;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;

@RestController
public class AppVersionController {

	@Value("${environment.val}")
	private String environmentVal;
	
	@RequestMapping(value = "/version", method = RequestMethod.GET,headers="Accept=application/json")	
	public ResponseEntity<JsonNode> getAppVersion(HttpServletRequest request, HttpServletResponse response) {   	
        
        InputStream inputStream = request.getServletContext().getResourceAsStream("/META-INF/MANIFEST.MF");    			
    	Manifest manifest=null;
    	ObjectMapper objectMapper = new ObjectMapper();
    	InetAddress ip;
        String hostname;

        Map<String, String> responseJson = new HashMap<>();
    	try {
    		ip = InetAddress.getLocalHost();
            hostname = ip.getHostName();
    		LocalTime time = LocalTime.now();    	    
			manifest = new Manifest(inputStream);
			java.util.jar.Attributes attributes = manifest.getMainAttributes();			
			responseJson.put("Application Name", attributes.getValue("Implementation-Title"));
			responseJson.put("Version", attributes.getValue("Implementation-Version"));
			responseJson.put("Build Timestamp", attributes.getValue("Build-Time"));	
			responseJson.put("Server Time Zone:", ZoneId.systemDefault().toString());
			responseJson.put("Server local time:", time+"");
			responseJson.put("Environment Variable:", environmentVal);
			responseJson.put("Host Name:", hostname);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
    	JsonNode jsonNode = objectMapper.valueToTree(responseJson);
    	response.setHeader("Access-Control-Allow-Origin", "*");
    	response.setContentType("application/json");
        return new ResponseEntity<>(jsonNode, HttpStatus.OK);
    }
}
