package com.slavic.springboot.filter;

import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.util.Arrays;
import java.util.Collections;
import java.util.Date;
import java.util.Enumeration;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.annotation.WebFilter;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
//import org.slf4j.Logger;
//import org.slf4j.LoggerFactory;
import org.springframework.web.util.ContentCachingRequestWrapper;
import org.springframework.web.util.ContentCachingResponseWrapper;
import org.springframework.web.util.WebUtils;

@WebFilter(urlPatterns = "/*")
public class HttpLoggingJbossFilter implements Filter {

    private static final Logger log = LogManager.getLogger(HttpLoggingJbossFilter.class);
    
    private static final Set<String> ALLOWED_PATHS = Collections.unmodifiableSet(new HashSet<>(
            Arrays.asList("/")));

    @Override
    public void init(FilterConfig filterConfig) throws ServletException {
    }
   

        @Override
        public void doFilter(ServletRequest servletRequest, ServletResponse servletResponse, FilterChain chain)
            throws IOException, ServletException {
		try {
			if (servletRequest instanceof HttpServletRequest && servletResponse instanceof HttpServletResponse) {
				HttpServletRequest request = (HttpServletRequest) servletRequest;
				HttpServletResponse response = (HttpServletResponse) servletResponse;
				String path = request.getRequestURI().substring(request.getContextPath().length()).replaceAll("[/]+$",
						"");
				String[] strPath = path.split("/");
				String appPath = "/" + strPath[1];
				boolean allowedPath = ALLOWED_PATHS.contains(appPath);
				if (allowedPath) {
					Map<String, String> requestMap = this.getTypesafeRequestMap(request);
					HttpServletRequest requestToCache = new ContentCachingRequestWrapper(request);
					HttpServletResponse responseToCache = new ContentCachingResponseWrapper(response);
					chain.doFilter(requestToCache, responseToCache);
					String requestData = getRequestData(requestToCache);
					String responseData = getResponseData(responseToCache);
					final StringBuilder logMessage = new StringBuilder("REST Request - ").append("[HTTP METHOD:")
							.append(request.getMethod()).append("] [PATH INFO:").append(request.getServletPath())
							.append("] [REQUEST PARAMETERS:").append(requestMap).append("] [REQUEST BODY:")
							.append(requestData).append("] [QUERY STRING:").append(request.getQueryString())
							.append("] [REMOTE ADDRESS:").append(request.getRemoteAddr()).append("]");
					log.debug("Request Data: " + logMessage);
					log.debug("Response Data: " + responseData);
				}else {
					chain.doFilter(servletRequest, servletResponse);
				}
			} else {
				chain.doFilter(servletRequest, servletResponse);
			}
		} catch (Throwable a) {
            	a.printStackTrace();
                log.error(a.getMessage());
            }
        }

        private static String getRequestData(final HttpServletRequest request) throws UnsupportedEncodingException {
            String payload = null;
            ContentCachingRequestWrapper wrapper = WebUtils.getNativeRequest(request, ContentCachingRequestWrapper.class);
            if (wrapper != null) {
                byte[] buf = wrapper.getContentAsByteArray();
                if (buf.length > 0) {
                    payload = new String(buf, 0, buf.length, wrapper.getCharacterEncoding());
                }
            }
            return payload;
        }

        private static String getResponseData(final HttpServletResponse response) throws IOException {
            String payload = null;
            ContentCachingResponseWrapper wrapper =
                WebUtils.getNativeResponse(response, ContentCachingResponseWrapper.class);
            if (wrapper != null) {
                byte[] buf = wrapper.getContentAsByteArray();
                if (buf.length > 0) {
                    payload = new String(buf, 0, buf.length, wrapper.getCharacterEncoding());
                    wrapper.copyBodyToResponse();
                }
            }
            return payload;
        }  

        @Override
        public void destroy() {
        }
        
        private Map<String, String> getTypesafeRequestMap(HttpServletRequest request) {
            Map<String, String> typesafeRequestMap = new HashMap<String, String>();
            Enumeration<?> requestParamNames = request.getParameterNames();
            while (requestParamNames.hasMoreElements()) {
                String requestParamName = (String) requestParamNames.nextElement();
                String requestParamValue;
                if (requestParamName.equalsIgnoreCase("password")) {
                    requestParamValue = "********";
                } else {
                    requestParamValue = request.getParameter(requestParamName);
                }
                typesafeRequestMap.put(requestParamName, requestParamValue);
            }
            return typesafeRequestMap;
        }
   
}