package com.slavic.springboot.dto;

import java.io.Serializable;
import java.sql.Date;
import java.util.List;

import javax.persistence.ElementCollection;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;


public class ParticipantsDTO implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	private Date updated;
	
	private ParticipantsDates start;
	
	private ParticipantsDates end;
	
	@JsonInclude(Include.NON_NULL)
	private String interval;
	
	@JsonInclude(Include.NON_NULL)
	private ParticipantsChanges changes;
	
	@JsonInclude(Include.NON_NULL)
	@ElementCollection
	private List<ParticipantsByInterval> participantsyInterval;
	

	public Date getUpdated() {
		return updated;
	}

	public void setUpdated(Date updated) {
		this.updated = updated;
	}

	public ParticipantsDates getStart() {
		return start;
	}

	public void setStart(ParticipantsDates start) {
		this.start = start;
	}

	public ParticipantsDates getEnd() {
		return end;
	}

	public void setEnd(ParticipantsDates end) {
		this.end = end;
	}

	public String getInterval() {
		return interval;
	}

	public void setInterval(String interval) {
		this.interval = interval;
	}

	public ParticipantsChanges getChanges() {
		return changes;
	}

	public void setChanges(ParticipantsChanges changes) {
		this.changes = changes;
	}

	public List<ParticipantsByInterval> getParticipantsyInterval() {
		return participantsyInterval;
	}

	public void setParticipantsyInterval(List<ParticipantsByInterval> participantsyInterval) {
		this.participantsyInterval = participantsyInterval;
	}
		
}

