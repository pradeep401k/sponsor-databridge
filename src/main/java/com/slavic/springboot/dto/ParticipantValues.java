package com.slavic.springboot.dto;

import java.io.Serializable;

public class ParticipantValues implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	private int total;
	private int contributing;
	private int eligible;
	private int withAssets;

	public ParticipantValues() {

	}

	public ParticipantValues(int total, int contributing, int eligible, int withAssets) {
		super();
		this.total = total;
		this.contributing = contributing;
		this.eligible = eligible;
		this.withAssets = withAssets;
	}

	public int getTotal() {
		return total;
	}

	public void setTotal(int total) {
		this.total = total;
	}

	public int getContributing() {
		return contributing;
	}

	public void setContributing(int contributing) {
		this.contributing = contributing;
	}

	public int getEligible() {
		return eligible;
	}

	public void setEligible(int eligible) {
		this.eligible = eligible;
	}

	public int getWithAssets() {
		return withAssets;
	}

	public void setWithAssets(int withAssets) {
		this.withAssets = withAssets;
	}

}
