package com.slavic.springboot.dto;

import java.io.Serializable;
import java.sql.Date;

import com.fasterxml.jackson.annotation.JsonProperty;

public class ParticipantsDates implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	
	private Date date;
	
	@JsonProperty("participants")
	private ParticipantValues partValues;
	
	public ParticipantsDates() {
	}
		
	public ParticipantsDates(Date date, ParticipantValues partValues) {
		this.date = date;
		this.partValues = partValues;
	}
	
	public Date getDate() {
		return date;
	}

	public void setDate(Date date) {
		this.date = date;
	}
}
