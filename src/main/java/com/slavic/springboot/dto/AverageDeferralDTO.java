package com.slavic.springboot.dto;

import java.sql.Date;
import java.util.List;


public class AverageDeferralDTO {

private Date updated;
	
	private RateDates start;
	
	private RateDates end;
	
	private String interval;
	
	
	private List<RatesByInterval> ratesByInterval;
	
	public Date getUpdated() {
		return updated;
	}


	public void setUpdated(Date updated) {
		this.updated = updated;
	}


	public RateDates getStart() {
		return start;
	}


	public void setStart(RateDates start) {
		this.start = start;
	}


	public RateDates getEnd() {
		return end;
	}


	public void setEnd(RateDates end) {
		this.end = end;
	}


	public String getInterval() {
		return interval;
	}


	public void setInterval(String interval) {
		this.interval = interval;
	}


	public List<RatesByInterval> getRatesByInterval() {
		return ratesByInterval;
	}


	public void setRatesByInterval(List<RatesByInterval> ratesByInterval) {
		this.ratesByInterval = ratesByInterval;
	}

	public class RatesByInterval{
		
		private Date date;
		private double rate;
	
		
		public RatesByInterval() {
			super();
		}
		public RatesByInterval(Date date, double rate ) {
			super();
			this.date = date;
			this.rate = rate;
		}
		
		public Date getDate() {
			return date;
		}
		public void setDate(Date date) {
			this.date = date;
		}
		public double getRate() {
			return rate;
		}
		public void setRate(double rate) {
			this.rate = rate;
		}	
		
	}
	
	public class RateDates{
		
		private Date date;
		
		private double rate;

		
		public RateDates() {
			super();
		}
		

		public RateDates(Date date, double rate) {
			super();
			this.date = date;
			this.rate = rate;
		}

		public Date getDate() {
			return date;
		}

		public void setDate(Date date) {
			this.date = date;
		}


		public double getRate() {
			return rate;
		}


		public void setRate(double rate) {
			this.rate = rate;
		}
		
	}
}
