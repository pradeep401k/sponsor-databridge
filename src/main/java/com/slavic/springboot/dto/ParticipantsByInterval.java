package com.slavic.springboot.dto;

import java.io.Serializable;
import java.sql.Date;
import java.time.LocalDate;

import com.fasterxml.jackson.annotation.JsonFormat;

public class ParticipantsByInterval implements Serializable{
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
//	@JsonFormat(pattern="MM/dd/yyyy")
	private Date date;
	private ParticipantValues participants;
	private ParticipantsChanges changes;
	
	public ParticipantsByInterval() {
		
	}
	public ParticipantsByInterval(Date date, ParticipantValues participants, ParticipantsChanges changes) {
		this.date = date;
		this.participants = participants;
		this.changes = changes;
	}
	public Date getDate() {
		return date;
	}
	public void setDate(Date date) {
		this.date = date;
	}
	public ParticipantValues getParticipants() {
		return participants;
	}
	public void setParticipants(ParticipantValues participants) {
		this.participants = participants;
	}
	public ParticipantsChanges getChanges() {
		return changes;
	}
	public void setChanges(ParticipantsChanges changes) {
		this.changes = changes;
	}
}
