package com.slavic.springboot.dto;

import com.fasterxml.jackson.annotation.JsonProperty;

public class ParticipantsChanges{
	
	@JsonProperty("newEligible")
	private int newEligibility;
	@JsonProperty("newEnrollments")
	private int newEnrollment;
	
	public ParticipantsChanges(){
	}
	
	public ParticipantsChanges(int newEligibility, int newEnrollment) {
		this.newEligibility = newEligibility;
		this.newEnrollment = newEnrollment;
	}

	public int getNewEligibility() {
		return newEligibility;
	}

	public void setNewEligibility(int newEligibility) {
		this.newEligibility = newEligibility;
	}

	public int getNewEnrollment() {
		return newEnrollment;
	}

	public void setNewEnrollment(int newEnrollment) {
		this.newEnrollment = newEnrollment;
	}
	
}
