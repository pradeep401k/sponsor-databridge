package com.slavic.springboot.dto;

import java.io.Serializable;
import java.sql.Date;
import java.util.List;

import javax.persistence.Entity;

import org.springframework.stereotype.Component;

import com.fasterxml.jackson.annotation.JsonProperty;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;




public class AssetsDTO implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	@ApiModelProperty( notes="As of date for data")
	private Date updated;
	
	private AssetsDates start;
	
	private AssetsDates end;
	
	private String interval;
	
	private AssetsChanges changes;
	
	private List<AssetsByInterval> assetsByInterval;

	public Date getUpdated() {
		return updated;
	}

	public void setUpdated(Date updated) {
		this.updated = updated;
	}

	public AssetsDates getStart() {
		return start;
	}

	public void setStart(AssetsDates start) {
		this.start = start;
	}

	public AssetsDates getEnd() {
		return end;
	}

	public void setEnd(AssetsDates end) {
		this.end = end;
	}

	public String getInterval() {
		return interval;
	}

	public void setInterval(String interval) {
		this.interval = interval;
	}

	public AssetsChanges getChanges() {
		return changes;
	}

	public void setChanges(AssetsChanges changes) {
		this.changes = changes;
	}

	public List<AssetsByInterval> getAssetsByInterval() {
		return assetsByInterval;
	}

	public void setAssetsByInterval(List<AssetsByInterval> assetsByInterval) {
		this.assetsByInterval = assetsByInterval;
	}
	
	
	public class AssetsByInterval {

		private Date dates;
		
		private double assets;
		

		public AssetsByInterval() {
			super();
		}

		public AssetsByInterval(Date dates, double assets, AssetsChanges changes) {
			super();
			this.dates = dates;
			this.assets = assets;
			this.changes = changes;
		}

		private AssetsChanges changes;

		
		public Date getDates() {
			return dates;
		}

		public void setDates(Date dates) {
			this.dates = dates;
		}

		public double getAssets() {
			return assets;
		}

		public void setAssets(double assets) {
			this.assets = assets;
		}

		public AssetsChanges getChanges() {
			return changes;
		}

		public void setChanges(AssetsChanges changes) {
			this.changes = changes;
		}
		
	}

	public class AssetsChanges {
		
		private double contributions;
		private double transferIn;
		private double distributions;
		private double transferOut;
		
		
		public AssetsChanges() {
			super();
		}
		
		public AssetsChanges(double contributions, double transferIn, double distributions, double transferOut) {
			super();
			this.contributions = contributions;
			this.transferIn = transferIn;
			this.distributions = distributions;
			this.transferOut = transferOut;
		}
		public double getContributions() {
			return contributions;
		}
		public void setContributions(double contributions) {
			this.contributions = contributions;
		}
		public double getTransferIn() {
			return transferIn;
		}
		public void setTransferIn(double transferIn) {
			this.transferIn = transferIn;
		}
		public double getDistributions() {
			return distributions;
		}
		public void setDistributions(double distributions) {
			this.distributions = distributions;
		}
		public double getTransferOut() {
			return transferOut;
		}
		public void setTransferOut(double transferOut) {
			this.transferOut = transferOut;
		}
		
	}
	
	public class AssetsDates {

		private Date date;
		
		private double assets;
		
		public AssetsDates() {
			super();
		}

		public AssetsDates(Date date, double assets) {
			super();
			this.date = date;
			this.assets = assets;
		}

		public Date getDate() {
			return date;
		}

		public void setDate(Date date) {
			this.date = date;
		}

		public double getAssets() {
			return assets;
		}

		public void setAssets(double assets) {
			this.assets = assets;
		}
		
	}

	
}
