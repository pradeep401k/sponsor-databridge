package com.slavic.springboot.dto;

import java.io.Serializable;
import java.sql.Date;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;

import com.fasterxml.jackson.annotation.JsonProperty;

import io.swagger.annotations.ApiModel;


public class PlansDTO implements Serializable {

	
	private static final long serialVersionUID = 1L;
	

	private Date updated;
	private PlanDates start;
	private PlanDates end;
	private String interval;
	private PlanChanges changes;
	private List<PlansByInterval> plansByInterval;
	
	public Date getUpdated() {
		return updated;
	}


	public void setUpdated(Date updated) {
		this.updated = updated;
	}


	public PlanDates getStart() {
		return start;
	}


	public void setStart(PlanDates start) {
		this.start = start;
	}


	public PlanDates getEnd() {
		return end;
	}


	public void setEnd(PlanDates end) {
		this.end = end;
	}


	public String getInterval() {
		return interval;
	}


	public void setInterval(String interval) {
		this.interval = interval;
	}


	public PlanChanges getChanges() {
		return changes;
	}


	public void setChanges(PlanChanges changes) {
		this.changes = changes;
	}

	
	

	public List<PlansByInterval> getPlansByInterval() {
		return plansByInterval;
	}


	public void setPlansByInterval(List<PlansByInterval> plansByInterval) {
		this.plansByInterval = plansByInterval;
	}




	public class PlansByInterval implements Serializable {
		
		/**
		 * 
		 */
		private static final long serialVersionUID = 1L;
		
		private Date date;
		private Plan plans;
		private PlanChanges changes;
		
		public PlansByInterval() {
			super();
		}
		public PlansByInterval(Date date, Plan plans, PlanChanges changes) {
			super();
			this.date = date;
			this.plans = plans;
			this.changes = changes;
		}
		public Date getDate() {
			return date;
		}
		public void setDate(Date date) {
			this.date = date;
		}
		public Plan getPlans() {
			return plans;
		}
		public void setPlans(Plan plans) {
			this.plans = plans;
		}
		public PlanChanges getChanges() {
			return changes;
		}
		public void setChanges(PlanChanges changes) {
			this.changes = changes;
		}
		
		
	}
	
	public class PlanChanges implements Serializable{
		
		/**
		 * 
		 */
		private static final long serialVersionUID = 1L;
		
		private int newPlans;
		private int mergeIn;
		private int mergeOut;
		
		PlanChanges(){
			super();
		}
		
		public PlanChanges(int newPlans, int mergeIn, int mergeOut) {
			super();
			this.newPlans = newPlans;
			this.mergeIn = mergeIn;
			this.mergeOut = mergeOut;
		}



		public int getNewPlans() {
			return newPlans;
		}
		public void setNewPlans(int newPlans) {
			this.newPlans = newPlans;
		}
		public int getMergeIn() {
			return mergeIn;
		}
		public void setMergeIn(int mergeIn) {
			this.mergeIn = mergeIn;
		}
		public int getMergeOut() {
			return mergeOut;
		}
		public void setMergeOut(int mergeOut) {
			this.mergeOut = mergeOut;
		}
		
	}

	public class PlanDates implements Serializable{
		
		/**
		 * 
		 */
		private static final long serialVersionUID = 1L;

		private Date date;
		
		private Plan plans;
		
		public PlanDates() {
			super();
		}
			
		public PlanDates(Date date, Plan plans) {
			super();
			this.date = date;
			this.plans = plans;
		}

		public Date getDate() {
			return date;
		}

		public void setDate(Date date) {
			this.date = date;
		}

		public Plan getPlans() {
			return plans;
		}

		public void setPlans(Plan plans) {
			this.plans = plans;
		}
	}
	
	public class Plan{
		
		private int total;
		private int safeHarbor;
		private int withAssets;
		private int contributingPlans;
		private int activePlans;

		
		public Plan() {
			super();
		}
		
//
//		public Plan(int total, int safeHarbor, int withAssets) {
//			super();
//			this.total = total;
//			this.safeHarbor = safeHarbor;
//			this.withAssets = withAssets;
//		}
		

		public Plan(int total, int safeHarbor, int withAssets, int contributingPlans, int activePlans) {
			super();
			this.total = total;
			this.safeHarbor = safeHarbor;
			this.withAssets = withAssets;
			this.contributingPlans = contributingPlans;
			this.activePlans = activePlans;
		}


		public int getTotal() {
			return total;
		}


		public void setTotal(int total) {
			this.total = total;
		}


		public int getSafeHarbor() {
			return safeHarbor;
		}


		public void setSafeHarbor(int safeHarbor) {
			this.safeHarbor = safeHarbor;
		}


		public int getWithAssets() {
			return withAssets;
		}


		public void setWithAssets(int withAssets) {
			this.withAssets = withAssets;
		}


		public int getContributingPlans() {
			return contributingPlans;
		}


		public void setContributingPlans(int contributingPlans) {
			this.contributingPlans = contributingPlans;
		}


		public int getActivePlans() {
			return activePlans;
		}


		public void setActivePlans(int activePlans) {
			this.activePlans = activePlans;
		}
		
		
		
		
	}
	
}
