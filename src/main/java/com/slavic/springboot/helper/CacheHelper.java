package com.slavic.springboot.helper;

import org.springframework.beans.BeansException;

import org.springframework.cache.Cache;

import org.springframework.cache.Cache.ValueWrapper;

import org.springframework.cache.CacheManager;

import org.springframework.context.ApplicationContext;

import org.springframework.context.ApplicationContextAware;

 

public class CacheHelper implements ApplicationContextAware {

 

  
  private static CacheManager CACHE_MANAGER;

 

  public static void putIntoCache(String cacheKey, String cacheName, Object obj) {

    final Cache cache = CACHE_MANAGER.getCache(cacheName);

    cache.put(cacheKey, obj);

  }

 

  public static Object getFromCache(String cacheKey, String cacheName) {

    final Cache cache = CACHE_MANAGER.getCache(cacheName);

    final ValueWrapper valueWrapper = cache.get(cacheKey);

    return (valueWrapper != null) ? valueWrapper.get() : null;

  }

 

  @Override

  public void setApplicationContext(ApplicationContext applicationContext) throws BeansException {

    CacheHelper.CACHE_MANAGER = applicationContext.getBean("cacheManager", CacheManager.class);

    System.out.println("CacheHelper.CACHE_MANAGER = " + CacheHelper.CACHE_MANAGER);

  }

}