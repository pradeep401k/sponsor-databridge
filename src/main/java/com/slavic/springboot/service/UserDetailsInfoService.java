package com.slavic.springboot.service;

import java.util.Map;

import com.slavic.springboot.bean.MyUser;

public interface UserDetailsInfoService {
	
	public Map<String, String> getUserDetailsInfo();
	
	public MyUser findByUserName(Map<String, String>userDetailsInfoMap, String userName);

}
