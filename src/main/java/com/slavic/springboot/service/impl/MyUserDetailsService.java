package com.slavic.springboot.service.impl;


import java.util.List;
import java.util.Map;

import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

import com.slavic.springboot.bean.MyUser;
import com.slavic.springboot.service.UserDetailsInfoService;

@Service("userDetailsService")
public class MyUserDetailsService implements UserDetailsService {

	@Autowired
	UserDetailsInfoService udis;

	@Override
	public UserDetails loadUserByUsername(final String userName) throws UsernameNotFoundException {
		
		MyUser user = null;		
		Map<String, String> userDetailsInfoMap = udis.getUserDetailsInfo();
		if(userDetailsInfoMap != null && StringUtils.isNotBlank(userName))
			user = udis.findByUserName(userDetailsInfoMap, userName);
		
		return buildUserForAuthentication(user, null);
		
	}

	private User buildUserForAuthentication(MyUser user, List<GrantedAuthority> authorities) {
		if(user != null) {
			PasswordEncoder passwordEncoder = new BCryptPasswordEncoder();
			String hashedPassword = passwordEncoder.encode(user.getPassword());
			return new User(user.getUsername(), hashedPassword, user.isEnabled(), true, true, true, user.getAuthorities());
		}
		return null;
	}

	/*private List<GrantedAuthority> buildUserAuthority(Set<UserRole> userRoles) {

		Set<GrantedAuthority> setAuths = new HashSet<GrantedAuthority>();

		// Build user's authorities
		for (UserRole userRole : userRoles) {
			setAuths.add(new SimpleGrantedAuthority(userRole.getRole()));
		}

		List<GrantedAuthority> Result = new ArrayList<GrantedAuthority>(setAuths);

		return Result;
	}*/

}