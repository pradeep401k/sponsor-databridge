package com.slavic.springboot.service.impl;

import java.sql.Date;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.slavic.springboot.dto.ParticipantValues;
import com.slavic.springboot.dto.ParticipantsByInterval;
import com.slavic.springboot.dto.ParticipantsChanges;
import com.slavic.springboot.dto.ParticipantsDTO;
import com.slavic.springboot.dto.ParticipantsDates;
import com.slavic.springboot.mysql.model.ParticipantChangesEntity;
import com.slavic.springboot.mysql.repository.ParticipantsChangesRepository;
import com.slavic.springboot.service.ParticipantService;



@Service
public class ParticipantsServiceImpl implements ParticipantService {

	private final Logger logger = LoggerFactory.getLogger(ParticipantsServiceImpl.class);
	private final static String MONTH= "month";
	private final static String YEAR = "year";
	private final static String QUARTER = "quarter";
	
	@Autowired
	ParticipantsChangesRepository partChangesRepo;
	
	List<ParticipantsChanges> partChangesByInterval = null;
	ParticipantsChanges p = null;
	List<ParticipantsByInterval> partInterval = null;
	ParticipantsByInterval pI = null;
	
	@Override
	public ParticipantsDTO getParticipantsStats(String peoId, LocalDate start, LocalDate end, String interval, int limit) {
		
		ParticipantsDTO participants = new ParticipantsDTO();
		partInterval = new ArrayList<ParticipantsByInterval>();
		
		ParticipantChangesEntity partStartChangeInfo = partChangesRepo.getInfoByPeoIdAndDate(peoId, start).orElse(new ParticipantChangesEntity());
		
		participants.setStart(new ParticipantsDates(Date.valueOf(start),new ParticipantValues(partStartChangeInfo.getTotalParticipants(),
				partStartChangeInfo.getContributing(),partStartChangeInfo.getEligible(),partStartChangeInfo.getWithAssets())));
		
		
		//participants.setChanges(new ParticipantsChanges(Integer.valueOf(partStartChangeInfo.getNewEligibleParticipants()), Integer.valueOf(partStartChangeInfo.getNewEnrollments())));
		
		ParticipantChangesEntity partEndChangeInfo = partChangesRepo.getInfoByPeoIdAndDate(peoId, end).orElse(new ParticipantChangesEntity());
		
		participants.setEnd(new ParticipantsDates(Date.valueOf(end),new ParticipantValues(partEndChangeInfo.getTotalParticipants(),
				partEndChangeInfo.getContributing(),partEndChangeInfo.getEligible(),partEndChangeInfo.getWithAssets())));	
		
		participants.setUpdated(Date.valueOf(LocalDate.now()));
		//participants.setStart(new ParticipantsDates(Date.valueOf(start),new ParticipantValues(participantsStart,0,0,0)));
		//participants.setEnd(new ParticipantsDates(Date.valueOf(end),new ParticipantValues(participantEnd,0,0,0)));	
		
		//participants.setChanges(new ParticipantsChanges(Integer.valueOf(partEndChangeInfo.getNewEligibleParticipants()), Integer.valueOf(partEndChangeInfo.getNewEnrollments())));
		
		if(interval != null) {
			List<ParticipantChangesEntity> partChangeByIntervalMonth = null;
			
			participants.setInterval(interval);
			if (interval.equals(MONTH)) {
				partChangeByIntervalMonth = partChangesRepo.getInfoByPeoIdAndMonth(peoId, start,end, limit);				
			}else if (interval.equals(QUARTER)) {
				partChangeByIntervalMonth = partChangesRepo.getInfoByPeoIdAndQuarter(peoId, start,end, limit); 
			}else if (interval.equals(YEAR)) {
				partChangeByIntervalMonth = partChangesRepo.getInfoByPeoIdAndYear(peoId, start,end, limit);
			}
			if (partChangeByIntervalMonth != null) {
				partChangesByInterval = new ArrayList<ParticipantsChanges>();
				partChangeByIntervalMonth.forEach(k -> {
					 p = new ParticipantsChanges(Integer.valueOf(k.getNewEligibleParticipants()),Integer.valueOf(k.getNewEnrollments()));
					 pI = new ParticipantsByInterval(Date.valueOf(k.getAsOfDate()),new ParticipantValues(k.getTotalParticipants(),k.getContributing(),k.getEligible(),k.getWithAssets()), p);
					 partInterval.add(pI);
				});
			}
			//partChangesByInterval =
			/*List<ParticipantChangesEntity> partChangeByIntervalMonth = partChangesRepo.getInfoByPeoIdAndMonth(peoId, start,end);
			partChangesByInterval = new ArrayList<ParticipantsChanges>();
			partChangeByIntervalMonth.forEach(k -> {
				 p = new ParticipantsChanges(Integer.valueOf(k.getNewEligibleParticipants()),Integer.valueOf(k.getNewEnrollments()));
				 pI = new ParticipantsByInterval(Date.valueOf(k.getAsOfDate()),new ParticipantValues(k.getTotalParticipants(),k.getContributing(),k.getEligible(),k.getWithAssets()), p);
				 partInterval.add(pI);
			});*/
			participants.setParticipantsyInterval(partInterval);
		}

		return participants;
	}

	@Override
	public ParticipantChangesEntity storeParticipantChanges(ParticipantChangesEntity participantChanges) {
		try {
			return partChangesRepo.save(participantChanges);
		}catch(Exception e) {
			logger.info("something went wrong while storeParticipantChanges..." + e.getMessage());
			return null;
		}
	}

}
