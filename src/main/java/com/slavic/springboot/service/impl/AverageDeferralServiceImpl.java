package com.slavic.springboot.service.impl;

import java.sql.Date;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.slavic.springboot.dto.AverageDeferralDTO;
import com.slavic.springboot.mysql.model.AverageDeferral;
import com.slavic.springboot.mysql.repository.AverageDeferralRepository;
import com.slavic.springboot.service.AverageDeferralService;




@Service
public class AverageDeferralServiceImpl implements AverageDeferralService{

	private final static String MONTH= "month";
	private final static String QUARTER= "quarter";
	private final static String YEAR= "year";
	
	AverageDeferralRepository averageDeferralRepository;
	
	
	public AverageDeferralServiceImpl(AverageDeferralRepository averageDeferralRepository) {
		super();
		this.averageDeferralRepository = averageDeferralRepository;
	}


	@Override
	public AverageDeferralDTO getAverageDeferralStats(String peoId, LocalDate start, LocalDate end, String interval, int limit) {
	
		AverageDeferralDTO rates = new AverageDeferralDTO();
		//TODO: how do we want to return default values Zero?
		LocalDate startAvgEndDate = null;
		
		// take the last of the month if not current month, if curr month take the curr date
		if (start.getMonth().equals(LocalDate.now().getMonth()) && start.getYear() == LocalDate.now().getYear()) {
			startAvgEndDate = LocalDate.now();
		} else {
			startAvgEndDate = start.withDayOfMonth(start.lengthOfMonth());
		}
		
		AverageDeferral startAvg = averageDeferralRepository.findByPeoIdAndStartDateAndEndDate(peoId, start,startAvgEndDate).orElse(new AverageDeferral());
		AverageDeferral endAvg = averageDeferralRepository.findByPeoIdAndStartDateAndEndDate(peoId, end.withDayOfMonth(1),end).orElse(new AverageDeferral());
		
		rates.setUpdated(Date.valueOf(LocalDate.now()));
		rates.setStart( rates.new RateDates(Date.valueOf(start), startAvg.getAverage()));
		rates.setEnd(rates.new RateDates(Date.valueOf(end), endAvg.getAverage()));		
		
		if(interval != null) {
			rates.setInterval(interval);
			List<AverageDeferralDTO.RatesByInterval> ratesList = new ArrayList<>();
			List<AverageDeferral> list = null;
			
			if(interval.equalsIgnoreCase(MONTH)) {
//				list = averageDeferralRepository.findByDateRange(peoId, start, end);
//				
//				ratesList =list.stream()
//					.filter(b -> b.getEndDate().getDayOfMonth() == b.getEndDate().lengthOfMonth() || b.getEndDate().getMonth() == LocalDate.now().getMonth()
//					&& b.getStartDate().getMonth() == b.getEndDate().getMonth() 
//					)
//					.limit(limit)
//					.map(m->{
//						return rates.new RatesByInterval(Date.valueOf(m.getEndDate()),m.getAverage());
//					}).collect(Collectors.toList());
				list = averageDeferralRepository.findByMonthRange(peoId, start, end, limit);
				
			}else if (interval.equalsIgnoreCase(QUARTER) || interval.equalsIgnoreCase(YEAR)) {
				
				if (interval.equalsIgnoreCase(YEAR)) {
					list = averageDeferralRepository.findByYearRange(peoId, start, end, limit);
				}else {
					list = averageDeferralRepository.findByQuarterRange(peoId, start, end, limit);
				}
			}
			if (list != null) {
				ratesList = list.stream()
						.map(m -> {
							return rates.new RatesByInterval(Date.valueOf(m.getEndDate()), m.getAverage());
						}).collect(Collectors.toList());
			}
			rates.setRatesByInterval(ratesList);	
		}
		return rates;		
	}
	
	public AverageDeferral getAverageDeferral(String peoId, LocalDate startDate, LocalDate endDate) {
		return averageDeferralRepository.findByPeoIdAndStartDateAndEndDate(peoId, startDate, endDate).orElse(new AverageDeferral());
		
	}
	
	public AverageDeferral storeAverageDeferral(AverageDeferral average) {
		
		
		return averageDeferralRepository.save(average);
	}
}
