package com.slavic.springboot.service.impl;

import java.sql.Date;
import java.time.LocalDate;
import java.time.temporal.TemporalAdjusters;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
import java.util.Optional;
import java.util.function.BiConsumer;
import java.util.stream.Collectors;

import org.springframework.stereotype.Service;

import com.slavic.springboot.dto.AssetsDTO;
import com.slavic.springboot.mysql.model.AssetsChanges;
import com.slavic.springboot.mysql.model.AssetsTotal;
import com.slavic.springboot.mysql.repository.AssetsChangesRepository;
import com.slavic.springboot.mysql.repository.AssetsTotalRepository;
import com.slavic.springboot.service.AssetsService;


@Service
public class AssetsServiceImpl implements AssetsService {

	
	private final static String MONTH= "month";
	private final static String YEAR= "year";
	private static final String QUARTER = "quarter";
	
	
	private AssetsTotalRepository assetsTotalRepository;
	
	private AssetsChangesRepository assetsChangesRepository;
	
	public AssetsServiceImpl(AssetsTotalRepository assetsTotalRepository,  AssetsChangesRepository assetsChangesRepository) {
		super();
		this.assetsTotalRepository = assetsTotalRepository;
		this.assetsChangesRepository = assetsChangesRepository;
	}

	@Override
	public AssetsDTO getAssetsStats(String peoId, LocalDate start, LocalDate end, String interval, int limit) {
		
		AssetsDTO assets = new AssetsDTO();
		
		double startTotal = assetsTotalRepository.findByPeoIdAndDate(peoId, start).orElse(new AssetsTotal()).getTotal().doubleValue();
		double endTotal=assetsTotalRepository.findByPeoIdAndDate(peoId, end).orElse(new AssetsTotal()).getTotal().doubleValue();
		
		assets.setUpdated(Date.valueOf(LocalDate.now().minusDays(1)));
		assets.setStart(assets.new AssetsDates(Date.valueOf(start),startTotal));
		assets.setEnd(assets.new AssetsDates(Date.valueOf(end),endTotal));
		
		if(interval != null) {
			AssetsChanges mainChanges = assetsChangesRepository.findByPeoIdAndStartAndEnd(peoId, start, end)
				.orElse(this.sumAssetChanges(peoId, start, end, interval));
		
		
			assets.setChanges(assets.new AssetsChanges(mainChanges.getContributions().doubleValue(), mainChanges.getTransferIn().doubleValue(),
				mainChanges.getDistributions().doubleValue(),mainChanges.getTransferOut().doubleValue()));
		
		
		
			/*if(interval.equals(YEAR)) {
				start = getDate(start, interval,true);//get the start of the year date
				end = getDate(end, interval,false); //get the end of the year date
			}*/
			assets.setInterval(interval);
			List<AssetsDTO.AssetsByInterval> assetList = new ArrayList<>();
			
			//get list from repository
			//List<AssetsChanges> changeList = assetsChangesRepository.findByRange(peoId, start, end);
			
			//travere through list and add values that fit the interval criteria
			if(interval.equalsIgnoreCase(MONTH)) {
				List<AssetsChanges> changeList = assetsChangesRepository.findByRange(peoId, start, end);
				
				assetList =changeList.stream().filter(d -> d.getEnd().getDayOfMonth() == d.getEnd().lengthOfMonth())
						.limit(limit)
						.map(a -> {
				return	assets.new AssetsByInterval(Date.valueOf(a.getEnd()),
						assetsTotalRepository.findByPeoIdAndDate(peoId, a.getEnd()).orElse(new AssetsTotal()).getTotal().doubleValue(),
							assets.new AssetsChanges(a.getContributions().doubleValue(),a.getTransferIn().doubleValue(),
									a.getDistributions().doubleValue(), a.getTransferOut().doubleValue()));
				}).collect(Collectors.toList());		
			}
			else if (interval.equalsIgnoreCase(YEAR) || interval.equalsIgnoreCase(QUARTER)) {
				List<AssetsChanges> changeList = null;
				
				if (interval.equalsIgnoreCase(YEAR)) {
					 changeList = assetsChangesRepository.findAssetChangesPerYear(peoId, start,end, limit);
				}else {
					changeList = assetsChangesRepository.findAssetsChangesByQuarter(peoId, start,end, limit);
				}
				assetList = changeList.stream().map(a ->{
					return	assets.new AssetsByInterval(Date.valueOf(a.getEnd()),
							 assetsTotalRepository.findByPeoIdAndDate(peoId,a.getEnd()).orElse(new AssetsTotal()).getTotal().doubleValue(),
					 assets.new AssetsChanges(a.getContributions().doubleValue(),a.getTransferIn().doubleValue(),a.getDistributions().doubleValue(),a.getTransferOut().doubleValue()));
							
				}).collect(Collectors.toList());
			}
			
			assets.setAssetsByInterval(assetList); 
		}
		return assets;
	}

	@Override
	public AssetsTotal getAssetsTotal(String peoId, LocalDate date) {

		Optional<AssetsTotal> assetsOptional = assetsTotalRepository.findByPeoIdAndDate(peoId, date);

		return assetsOptional.orElse(new AssetsTotal());
	}
	
	@Override
	public AssetsChanges getAssetsChange(String peoId, LocalDate start,
			LocalDate end) {

		Optional<AssetsChanges> changesOptional = assetsChangesRepository.findByPeoIdAndStartAndEnd(peoId, start, end);
		
		return changesOptional.orElse(new AssetsChanges());
	}

	@Override
	public AssetsTotal storeAssetsTotal(AssetsTotal assetsTotal) {
		return assetsTotalRepository.save(assetsTotal);
		
	}

	

	@Override
	public AssetsChanges storeAssetChanges(AssetsChanges changes) {
		return assetsChangesRepository.save(changes);
	}
	
	//the addition is happening at at monthly level, we are filterig dates by checking teh
		//end date matches the last day of the month in the range and we are also 
			//checking that the interval object is in the same month start and end 
	public AssetsChanges sumAssetChanges(String peoId, LocalDate start, LocalDate end, String interval) {
		
		List<AssetsChanges> changeList = assetsChangesRepository.findByRange(peoId, start, end);
		
		BiConsumer<AssetsChanges, AssetsChanges> reducer = (o1, o2) -> {
		    o1.setContributions(o1.getContributions().add(o2.getContributions()));
		    o1.setDistributions(o1.getDistributions().add(o2.getDistributions()));
		    o1.setTransferIn(o1.getTransferIn().add(o2.getTransferIn()));
		    o1.setTransferOut(o1.getTransferOut().add(o2.getTransferOut()));

		};
		
		if (interval.contentEquals(QUARTER) || interval.contentEquals(YEAR)) {
			return changeList.stream().filter(c-> (
					(c.getStart().getMonth() == c.getEnd().getMonth() //same month
					& (c.getStart().lengthOfMonth() == (c.getStart().lengthOfMonth() - c.getStart().getDayOfMonth()) +1)// one month from start -end
					&& c.getEnd().getDayOfMonth()==c.getStart().lengthOfMonth()))) //check end date
					.collect(()-> new AssetsChanges(), reducer, reducer);
		}
		//get the max date for end date
		AssetsChanges  maxChangeAvailable =changeList.stream().max(Comparator.comparing(AssetsChanges::getEnd)).get();
		
		//this will work up to the last available day of the end month
		return changeList.stream().filter(c-> (
				(c.getEnd().getDayOfMonth()== c.getEnd().lengthOfMonth() && c.getStart().getMonth() == c.getEnd().getMonth() && c.getEnd().isBefore(end) ) 
				|| (c.getEnd() == maxChangeAvailable.getEnd()))) 
				.collect(()-> new AssetsChanges(), reducer, reducer);
		
	}
	
	private LocalDate getDate(LocalDate myDate, String interval, boolean isStartDate) {
		if (interval.equalsIgnoreCase(MONTH)) {
			return myDate;
		}else if (interval.equalsIgnoreCase(YEAR)) {
			return  isStartDate ? myDate.with(TemporalAdjusters.firstDayOfYear()) : myDate.with(TemporalAdjusters.lastDayOfYear());
		}else if (interval.equals(QUARTER)) {
			return isStartDate ? myDate.with(myDate.getMonth().firstMonthOfQuarter()).with(TemporalAdjusters.firstDayOfMonth())
					:myDate.plusMonths(2) .with(TemporalAdjusters.lastDayOfMonth());
		}
		return null;
	}

}
