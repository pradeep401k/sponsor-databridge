package com.slavic.springboot.service.impl;

import java.sql.Date;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;
import java.util.function.BiConsumer;
import org.springframework.stereotype.Service;

import com.slavic.springboot.dto.PlansDTO;
import com.slavic.springboot.mysql.model.PlansChanges;
import com.slavic.springboot.mysql.model.PlansTotal;
import com.slavic.springboot.mysql.repository.PlansChangesRepository;
import com.slavic.springboot.mysql.repository.PlansTotalRepository;
import com.slavic.springboot.service.PlansService;

@Service
public class PlansServiceImpl implements PlansService {

	private PlansChangesRepository planChangesRepository;
	
	private PlansTotalRepository plansTotalRepository;
	
	private final static String MONTH= "month";
	private final static String YEAR = "year";
	private final static String QUARTER = "quarter";

	public PlansServiceImpl(PlansChangesRepository planChangesRepository, PlansTotalRepository plansTotalRepository) {
		super();
		this.planChangesRepository = planChangesRepository;
		this.plansTotalRepository = plansTotalRepository;
	}


	@Override
	public PlansDTO getPlansStats(String peoId, LocalDate start, LocalDate end, String interval, int limit) {

		PlansDTO plans = new PlansDTO();
		
		PlansTotal startTotal = plansTotalRepository.findByPeoIdAndDate(peoId, start).orElse(new PlansTotal());
		PlansTotal endTotal = plansTotalRepository.findByPeoIdAndDate(peoId, end).orElse(new PlansTotal());
		
		plans.setUpdated(Date.valueOf(LocalDate.now()));
		plans.setStart( plans.new PlanDates(Date.valueOf(start), plans.new Plan(startTotal.getTotal(), startTotal.getSafeHarborPlans(),
				startTotal.getAssetsPlans(),startTotal.getContribPlans(), startTotal.getActivePlans())));
		plans.setEnd(plans.new PlanDates(Date.valueOf(end),plans.new Plan(endTotal.getTotal(), endTotal.getSafeHarborPlans(),
				endTotal.getAssetsPlans(),startTotal.getContribPlans(), startTotal.getActivePlans())));
		
		
		if(interval != null) {
			PlansChanges changes = this.sumPlansChanges(peoId, start, end, interval);
			plans.setChanges(plans.new PlanChanges(changes.getNewPlans(), changes.getMergeIn(),changes.getMergeOut()));
			
			plans.setInterval(interval);
			List<PlansDTO.PlansByInterval> planList = new ArrayList<>();
			//get list from repository
			List<PlansChanges> changeList = null;//planChangesRepository.findByRange(peoId, start, end);
			
			//travere through list and add values that fit the interval criteria
			if(interval.equalsIgnoreCase(MONTH)) {
				int row = 0;
				//changeList = planChangesRepository.findByRange(peoId, start, end);
				changeList = planChangesRepository.findByMonthRange(peoId, start, end, limit);
				for(PlansChanges plan: changeList) {
					//if (row >= limit)break;
					//if( plan.getEnd().getDayOfMonth() == plan.getEnd().lengthOfMonth()) {
						//row++;
						PlansTotal total = plansTotalRepository.findByPeoIdAndDate(peoId, plan.getEnd()).orElse(new PlansTotal());
						planList.add(	plans.new PlansByInterval(Date.valueOf(plan.getEnd()),
								plans.new Plan(total.getTotal(), total.getSafeHarborPlans(), total.getAssetsPlans(),
								total.getContribPlans(),total.getActivePlans()),
								plans.new PlanChanges(plan.getNewPlans(),plan.getMergeIn(),plan.getMergeOut())));
					//}
					
				}
			}else if (interval.equalsIgnoreCase(QUARTER) || interval.equalsIgnoreCase(YEAR)) {
				
				if (interval.equalsIgnoreCase(QUARTER)) {
					changeList = planChangesRepository.findByQuarterRange(peoId, start, end, limit);
				}else {
					changeList = planChangesRepository.findByYearRange(peoId, start, end, limit);
				}
				for(PlansChanges plan: changeList) {
					PlansTotal total = plansTotalRepository.findByPeoIdAndDate(peoId, plan.getEnd()).orElse(new PlansTotal());
					planList.add(plans.new PlansByInterval(Date.valueOf(plan.getEnd()),
								plans.new Plan(total.getTotal(),total.getSafeHarborPlans(),total.getAssetsPlans(),total.getContribPlans(),total.getActivePlans()),
								 plans.new PlanChanges(plan.getNewPlans(),plan.getMergeIn(),plan.getMergeOut())));
				}
			}	
			plans.setPlansByInterval(planList);
		}
		
		return plans;
	}
	
public PlansChanges sumPlansChanges(String peoId, LocalDate start, LocalDate end, String interval) {
		
		List<PlansChanges> changeList=  planChangesRepository.findByRange(peoId, start, end);;
		
		BiConsumer<PlansChanges, PlansChanges> reducer = (o1, o2) -> {
		    o1.setMergeIn(o1.getMergeIn()+o2.getMergeIn());
		    o1.setMergeOut(o1.getMergeOut()+o2.getMergeOut());
		    o1.setNewPlans(o1.getNewPlans()+ o2.getNewPlans());
		};
		
		if (interval.equals(QUARTER) || interval.equals(YEAR)) {
			
			return changeList.stream().filter(c-> (
					(c.getStart().getMonth() == c.getEnd().getMonth() //same month
					&& (c.getStart().lengthOfMonth() == (c.getStart().lengthOfMonth() - c.getStart().getDayOfMonth()) +1)//one month from start -end dates
					&& c.getEnd().getDayOfMonth()==c.getStart().lengthOfMonth()))) //check end date
					.collect(()-> new PlansChanges(), reducer, reducer);
		}
		return changeList.stream().filter(c-> ( (c.getEnd().getDayOfMonth()== c.getEnd().lengthOfMonth() 	
				&& c.getStart().getMonth() == c.getEnd().getMonth() && c.getEnd().isBefore(end) ) 
				|| (c.getEnd().getMonth() == end.getMonth() && c.getEnd().getDayOfMonth() == end.getDayOfMonth() )
				)) //this works for month, but it wont work for quarters
				.collect(()-> new PlansChanges(), reducer, reducer);
	}


@Override
public PlansTotal storePlansTotal(PlansTotal plansTotal) {
	
	return plansTotalRepository.save(plansTotal);
}


@Override
public PlansChanges storePlansChanges(PlansChanges plansChanges) {
	
	return planChangesRepository.save(plansChanges);
}

	

}
