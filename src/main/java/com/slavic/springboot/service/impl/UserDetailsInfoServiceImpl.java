package com.slavic.springboot.service.impl;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.stereotype.Component;

import com.slavic.springboot.bean.MyUser;
import com.slavic.springboot.mysql.model.UserDetailsInfo;
import com.slavic.springboot.mysql.repository.UserDetailsInfoRepository;
import com.slavic.springboot.service.UserDetailsInfoService;


@Component
public class UserDetailsInfoServiceImpl implements UserDetailsInfoService{

	@Autowired
	UserDetailsInfoRepository udir;
	
	private Map<String, String> userDetailsInfoMap = null;	
	
	@Cacheable(value="userdetailsinfo.list")
	public Map<String, String> getUserDetailsInfo() {		
		List<UserDetailsInfo> udiList = udir.getAllUserInfo();
		
		userDetailsInfoMap = new HashMap<String, String>();
		for(UserDetailsInfo udi : udiList){			
			if(udi != null && StringUtils.isNotBlank(udi.getUserid()))
				userDetailsInfoMap.put(udi.getAdminid()+"_"+udi.getUserid(), udi.getPassword());
		}		
		return userDetailsInfoMap;
	}
	
	
	public MyUser findByUserName(Map<String, String>userDetailsInfoMap, String userName) {		
		MyUser myUser = null;
		if(StringUtils.isNotBlank(userName)) {			
			if(userDetailsInfoMap != null && userDetailsInfoMap.containsKey(userName)) {
				myUser = new MyUser();
				//myUser.setUsername(userName.substring(userName.indexOf("_")+1));
				myUser.setUsername(userName);
				myUser.setPassword(userDetailsInfoMap.get(userName));
				myUser.setEnabled(true);
			}
		}
		return myUser;
	}

}
