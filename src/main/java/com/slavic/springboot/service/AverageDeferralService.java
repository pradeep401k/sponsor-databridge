package com.slavic.springboot.service;

import java.time.LocalDate;

import com.slavic.springboot.dto.AverageDeferralDTO;
import com.slavic.springboot.mysql.model.AverageDeferral;


public interface AverageDeferralService {

	public AverageDeferralDTO getAverageDeferralStats(String peoId, LocalDate start, LocalDate end, String interval, int limit);
	
	public AverageDeferral getAverageDeferral(String peoId, LocalDate startDate, LocalDate endDate);
	
	public AverageDeferral storeAverageDeferral(AverageDeferral average);
	
	
	
}
