package com.slavic.springboot.service;

import java.time.LocalDate;

import com.slavic.springboot.dto.ParticipantsDTO;
import com.slavic.springboot.mysql.model.ParticipantChangesEntity;



public interface ParticipantService {
	public ParticipantsDTO getParticipantsStats(String peoId, LocalDate start, LocalDate end, String interval, int limit);

	public ParticipantChangesEntity storeParticipantChanges(ParticipantChangesEntity participantChanges);
}
