package com.slavic.springboot.service;

import java.time.LocalDate;

import com.slavic.springboot.dto.PlansDTO;
import com.slavic.springboot.mysql.model.PlansChanges;
import com.slavic.springboot.mysql.model.PlansTotal;

public interface PlansService {

	public PlansDTO getPlansStats(String peoId, LocalDate start, LocalDate end, String interval, int limit);
	
	public PlansTotal storePlansTotal (PlansTotal plansTotal) ;
	
	public PlansChanges storePlansChanges (PlansChanges plansChanges);
	
}
