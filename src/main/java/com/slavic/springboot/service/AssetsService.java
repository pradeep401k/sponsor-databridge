package com.slavic.springboot.service;

import java.time.LocalDate;

import com.slavic.springboot.dto.AssetsDTO;
import com.slavic.springboot.mysql.model.AssetsChanges;
import com.slavic.springboot.mysql.model.AssetsTotal;

public interface AssetsService {

	public AssetsDTO getAssetsStats(String peoId, LocalDate start, LocalDate end,  String interval, int limit);
	
	public AssetsTotal getAssetsTotal(String peoId, LocalDate date);
	
	public AssetsChanges getAssetsChange(String peoId, LocalDate start, LocalDate end);
	
	public AssetsTotal storeAssetsTotal(AssetsTotal assetsTotal);
	
	public AssetsChanges storeAssetChanges(AssetsChanges changes);
	
	public AssetsChanges sumAssetChanges(String peoId, LocalDate start, LocalDate end, String interval);
	
}
