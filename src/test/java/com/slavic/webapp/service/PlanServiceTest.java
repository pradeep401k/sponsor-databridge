package com.slavic.webapp.service;

import static org.junit.jupiter.api.Assertions.*;


import org.junit.jupiter.api.Test;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;
import org.mockito.Mockito;

import com.slavic.springboot.dto.PlansDTO;
import com.slavic.springboot.dto.PlansDTO.PlansByInterval;
import com.slavic.springboot.mysql.model.PlansChanges;
import com.slavic.springboot.mysql.model.PlansTotal;
import com.slavic.springboot.mysql.repository.PlansChangesRepository;
import com.slavic.springboot.mysql.repository.PlansTotalRepository;
import com.slavic.springboot.service.PlansService;
import com.slavic.springboot.service.impl.PlansServiceImpl;

import org.junit.jupiter.api.Test;

//import com.slavic.springboot.dto.PlansDTO;
//import com.slavic.springboot.dto.PlansDTO.Plan;
//import com.slavic.springboot.dto.PlansDTO.PlanChanges;
//import com.slavic.springboot.dto.PlansDTO.PlanDates;
//import com.slavic.springboot.dto.PlansDTO.PlansByInterval;
//import com.slavic.springboot.mysql.model.PlansChanges;
//import com.slavic.springboot.mysql.model.PlansTotal;
//import com.slavic.springboot.mysql.repository.PlansChangesRepository;
//import com.slavic.springboot.mysql.repository.PlansTotalRepository;
//import com.slavic.webapp.service.impl.PlansServiceImpl;


import java.time.LocalDate;
import java.util.ArrayList;

import java.util.List;
import java.util.Optional;

class PlanServiceTest {
	
	private PlansChangesRepository planChangesRepository = mock(PlansChangesRepository.class);	
	private PlansTotalRepository plansTotalRepository = mock(PlansTotalRepository.class);

	

	@Test
	public void test_get_plan_stats_start_end_totals() {
		String peoId = "test";
		LocalDate start = LocalDate.of(2019, 01, 01);	
		LocalDate end = LocalDate.of(2019, 04, 30);	
		
		PlansDTO plans = new PlansDTO();
		
		PlansTotal pt = new PlansTotal();
		pt.setPeoId("test");
		pt.setActivePlans(12);
		pt.setTotal(100);
		pt.setSafeHarborPlans(1);
		pt.setAssetsPlans(11);
		pt.setContribPlans(5);
		
		PlansTotal pt2 = new PlansTotal();
		pt2.setPeoId("test");
		pt2.setActivePlans(12);
		pt2.setTotal(200);
		pt2.setSafeHarborPlans(1);
		pt2.setAssetsPlans(11);
		pt2.setContribPlans(5);
		
		List<PlansChanges> changeList= new ArrayList<>();
		PlansChanges changes = new PlansChanges();
		changes.setNewPlans(100);
		changes.setMergeIn(10);
		changes.setMergeOut(1);
		changes.setEnd(end);
		changeList.add(changes);
		
		//start total		
		Mockito.when(plansTotalRepository.findByPeoIdAndDate(peoId, start)).thenReturn(Optional.of(pt));
		//end total
		Mockito.when(plansTotalRepository.findByPeoIdAndDate(peoId, end)).thenReturn(Optional.of(pt2));
		//Sum of changes by quarter
		Mockito.when(planChangesRepository.findByQuarterRange(peoId, start, end, 1)).thenReturn(changeList);
		
		PlansService service = new PlansServiceImpl(planChangesRepository,plansTotalRepository);
		//plans = service.getPlansStats(peoId, start, end, "quarter");
		plans = service.getPlansStats(peoId, start, end, "year", 2);
		//plans = service.getPlansStats(peoId, start, end, "month");
		
		PlansDTO.PlanDates dto = plans.getStart();
		double startTotal =dto.getPlans().getTotal();
		
		assertEquals(startTotal, 100);
		
		dto = plans.getEnd();
		double endTotal =dto.getPlans().getTotal();
		assertEquals(endTotal, 200);
	}
	
	@Test
	public void test_get_plan_stats_changes() {
		String peoId = "test";
		LocalDate start = LocalDate.of(2019, 01, 01);	
		LocalDate end = LocalDate.of(2019, 04, 30);	
		
		PlansDTO plans = new PlansDTO();
		
		PlansTotal pt = new PlansTotal();
		pt.setPeoId("test");
		pt.setActivePlans(12);
		pt.setTotal(100);
		pt.setSafeHarborPlans(1);
		pt.setAssetsPlans(11);
		pt.setContribPlans(5);
		
		PlansTotal pt2 = new PlansTotal();
		pt2.setPeoId("test");
		pt2.setActivePlans(12);
		pt2.setTotal(200);
		pt2.setSafeHarborPlans(1);
		pt2.setAssetsPlans(11);
		pt2.setContribPlans(5);
		
		List<PlansChanges> changeList= new ArrayList<>();
		PlansChanges changes = new PlansChanges();
		changes.setNewPlans(100);
		changes.setMergeIn(10);
		changes.setMergeOut(1);
		changes.setEnd(end);
		changeList.add(changes);
		
		//start total		
		Mockito.when(plansTotalRepository.findByPeoIdAndDate(peoId, start)).thenReturn(Optional.of(pt));
		//end total
		Mockito.when(plansTotalRepository.findByPeoIdAndDate(peoId, end)).thenReturn(Optional.of(pt2));
		//Sum of changes by quarter
		Mockito.when(planChangesRepository.findByQuarterRange(peoId, start, end, 2)).thenReturn(changeList);
		//sum of changes by year
		Mockito.when(planChangesRepository.findByYearRange(peoId, start, end, 2)).thenReturn(changeList);
		
		PlansService service = new PlansServiceImpl(planChangesRepository,plansTotalRepository);
		//plans = service.getPlansStats(peoId, start, end, "quarter");
		plans = service.getPlansStats(peoId, start, end, "year", 1);
		
		PlansDTO.PlanChanges changesDTO = plans.getChanges();
		double mergeIns = changesDTO.getMergeIn();
		double newPlans = changesDTO.getNewPlans();
		double mergeOuts= changesDTO.getMergeOut();
		
		assertEquals(mergeIns, 10);
		assertEquals(newPlans, 100);
		assertEquals(mergeOuts, 1);
		
	}
	
	@Test
	public void test_get_plan_stats_changes_interval() {
		String peoId = "test";
		LocalDate start = LocalDate.of(2019, 01, 01);	
		LocalDate end = LocalDate.of(2019, 04, 30);	
		
		PlansDTO plans = new PlansDTO();
		
		PlansTotal pt = new PlansTotal();
		pt.setPeoId("test");
		pt.setActivePlans(12);
		pt.setTotal(100);
		pt.setSafeHarborPlans(1);
		pt.setAssetsPlans(11);
		pt.setContribPlans(5);
		
		PlansTotal pt2 = new PlansTotal();
		pt2.setPeoId("test");
		pt2.setActivePlans(12);
		pt2.setTotal(200);
		pt2.setSafeHarborPlans(1);
		pt2.setAssetsPlans(11);
		pt2.setContribPlans(5);
		
		List<PlansChanges> changeList= new ArrayList<>();
		PlansChanges changes = new PlansChanges();
		changes.setNewPlans(100);
		changes.setMergeIn(10);
		changes.setMergeOut(1);
		changes.setEnd(end);
		changeList.add(changes);
		
		//List<PlansByInterval> planList2 = new ArrayList<>();
		List<PlansDTO.PlansByInterval> planList = new ArrayList<>();
		
		java.sql.Date sqlDate = java.sql.Date.valueOf(start);
		PlansDTO.Plan p = plans.new Plan(12,34,12,44,500);
		PlansDTO.PlanChanges c = plans.new PlanChanges(1,2,2);
		
		PlansDTO.PlansByInterval e = plans.new PlansByInterval(sqlDate, p, c);
		planList.add(e);
		

		
		//start total		
		Mockito.when(plansTotalRepository.findByPeoIdAndDate(peoId, start)).thenReturn(Optional.of(pt));
		//end total
		Mockito.when(plansTotalRepository.findByPeoIdAndDate(peoId, end)).thenReturn(Optional.of(pt2));
		//Sum of changes by quarter
		Mockito.when(planChangesRepository.findByQuarterRange(peoId, start, end, 1)).thenReturn(changeList);
		//sum of changes by year
		Mockito.when(planChangesRepository.findByYearRange(peoId, start, end, 1)).thenReturn(changeList);
		
		PlansService service = new PlansServiceImpl(planChangesRepository,plansTotalRepository);
		//plans = service.getPlansStats(peoId, start, end, "quarter");
		plans = service.getPlansStats(peoId, start, end, "year", 2);
		
		
		 planList =plans.getPlansByInterval();
		 PlansByInterval planBI = planList.get(0);
		
		assertEquals(planBI.getPlans().getTotal(), 200);
		assertEquals(planBI.getPlans().getSafeHarbor(), 1);
		assertEquals(planBI.getPlans().getWithAssets(), 11);
		assertEquals(planBI.getPlans().getContributingPlans(), 5);
		assertEquals(planBI.getPlans().getActivePlans(), 12);
		
		
		
		
	}

}
