package com.slavic.webapp.service;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

import java.math.BigDecimal;
import java.sql.Date;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import org.junit.jupiter.api.Test;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.springframework.boot.test.mock.mockito.MockBean;

import com.slavic.springboot.dto.AssetsDTO;
import com.slavic.springboot.mysql.model.AssetsChanges;
import com.slavic.springboot.mysql.model.AssetsTotal;
import com.slavic.springboot.mysql.repository.AssetsChangesRepository;
import com.slavic.springboot.mysql.repository.AssetsTotalRepository;
import com.slavic.springboot.service.AssetsService;
import com.slavic.springboot.service.impl.AssetsServiceImpl;




class AssetsServiceTest {

	//@MockBean
	private AssetsTotalRepository totalRepo = mock(AssetsTotalRepository.class);
	//@MockBean
	private AssetsChangesRepository changeRepo = mock(AssetsChangesRepository.class);
	
	
	@Test
	public void testAssetsEntity() {
		AssetsTotal assets = new AssetsTotal();
		
		assets.setPeoId("Test");
		assets.setDate(LocalDate.parse("2020-04-03"));
		
		assertEquals("Test", assets.getPeoId());
		assertEquals("2020-04-03", assets.getDate().format(DateTimeFormatter.ofPattern("yyyy-MM-dd")));
	}
	
	//assert we are getting a begin total and end total asset - full return DTO
	
	@Test
	public void test_changes_happy_path() {
		
		String peoId = "test";
		LocalDate start = LocalDate.of(2020, 04, 01);	
		LocalDate end = LocalDate.of(2020, 04, 30);	
		BigDecimal contributions = new BigDecimal(3_345_345.00);
		
		AssetsChanges originalChanges = new AssetsChanges();
		
		originalChanges.setPeoId(peoId);
		originalChanges.setStart(start);
		originalChanges.setEnd(end);
		originalChanges.setContributions(new BigDecimal(3_345_345.00));
		originalChanges.setDistributions(contributions);
		
		when(changeRepo.findByPeoIdAndStartAndEnd(peoId, start,end)).thenReturn(Optional.of(originalChanges));
		
		AssetsService service= new AssetsServiceImpl(totalRepo, changeRepo);

		AssetsChanges changes = service.getAssetsChange(peoId, start, end);
	
		assertEquals(contributions.doubleValue(), changes.getContributions().doubleValue());
		assertEquals(peoId, changes.getPeoId());
		
	}
	
	//test addition method for changes
	@Test
	public void test_sum_method_changes_happy_path() {
		
		String peoId = "test";
		LocalDate start = LocalDate.of(2020, 01, 01);	
		LocalDate end = LocalDate.of(2020, 04, 29);	
		
		List<AssetsChanges> changeList = new ArrayList<>();
		
		//changeList.add(new AssetsChanges(peoId, LocalDate.of(2020, 04, 01), end ,new BigDecimal(250) ,new BigDecimal(520),new BigDecimal(450),new BigDecimal(550)));
		changeList.add(new AssetsChanges(peoId,  LocalDate.of(2020, 04, 01),  LocalDate.of(2020, 04, 29) ,new BigDecimal(220) ,new BigDecimal(520),new BigDecimal(450),new BigDecimal(550)));
		changeList.add(new AssetsChanges(peoId,  LocalDate.of(2020, 01, 01),  LocalDate.of(2020, 01, 31) ,new BigDecimal(250) ,new BigDecimal(520),new BigDecimal(450),new BigDecimal(550)));
		changeList.add(new AssetsChanges(peoId,  LocalDate.of(2020, 02, 01),  LocalDate.of(2020, 02, 29) ,new BigDecimal(250) ,new BigDecimal(520),new BigDecimal(450),new BigDecimal(550)));
		changeList.add(new AssetsChanges(peoId,  LocalDate.of(2020, 03, 01),  LocalDate.of(2020, 03, 31) ,new BigDecimal(250) ,new BigDecimal(520),new BigDecimal(450),new BigDecimal(550)));
		changeList.add(new AssetsChanges(peoId,  LocalDate.of(2020, 02, 01),  LocalDate.of(2020, 02, 02) ,new BigDecimal(50) ,new BigDecimal(520),new BigDecimal(450),new BigDecimal(550)));
		changeList.add(new AssetsChanges(peoId,  LocalDate.of(2020, 01, 01),  LocalDate.of(2020, 02, 29) ,new BigDecimal(100) ,new BigDecimal(520),new BigDecimal(450),new BigDecimal(550)));

		when(changeRepo.findByRange(peoId, start, end)).thenReturn(changeList);
		when(changeRepo.findByPeoIdAndStartAndEnd(peoId, start, end)).thenReturn(Optional.empty());
		
		when(totalRepo.findByPeoIdAndDate(peoId, start)).thenReturn(Optional.of(new AssetsTotal(peoId,start,new BigDecimal(234))));
		when(totalRepo.findByPeoIdAndDate(peoId, end)).thenReturn(Optional.of(new AssetsTotal(peoId,end,new BigDecimal(500))));
		when(totalRepo.findByPeoIdAndDate(peoId,  LocalDate.of(2020, 01, 31))).thenReturn(Optional.of(new AssetsTotal(peoId,end,new BigDecimal(500))));
		when(totalRepo.findByPeoIdAndDate(peoId, LocalDate.of(2020, 02, 29))).thenReturn(Optional.of(new AssetsTotal(peoId,end,new BigDecimal(500))));
		when(totalRepo.findByPeoIdAndDate(peoId,  LocalDate.of(2020, 03, 31))).thenReturn(Optional.of(new AssetsTotal(peoId,end,new BigDecimal(500))));
		
		
		AssetsService service= new AssetsServiceImpl(totalRepo, changeRepo);
		
		AssetsDTO dto = service.getAssetsStats(peoId, start, end, "month", 2);
		
		assertEquals( 970.0, dto.getChanges().getContributions() );
		
	}
	
	@Test
	public void test_sum_assets_same_month() {
		
		String peoId = "test";
		LocalDate start = LocalDate.of(2020, 01, 01);	
		LocalDate end = LocalDate.of(2020, 04, 30);	
		
	List<AssetsChanges> changeList = new ArrayList<>();
		
		changeList.add(new AssetsChanges(peoId, LocalDate.of(2020, 04, 01),  LocalDate.of(2020, 04, 30) ,new BigDecimal(220) ,new BigDecimal(520),new BigDecimal(450),new BigDecimal(550)));
		changeList.add(new AssetsChanges(peoId,  LocalDate.of(2020, 04, 01),  LocalDate.of(2020, 04, 29) ,new BigDecimal(220) ,new BigDecimal(520),new BigDecimal(450),new BigDecimal(550)));
		changeList.add(new AssetsChanges(peoId,  LocalDate.of(2020, 04, 01),  LocalDate.of(2020, 01, 01) ,new BigDecimal(250) ,new BigDecimal(520),new BigDecimal(450),new BigDecimal(550)));
		changeList.add(new AssetsChanges(peoId,  LocalDate.of(2020, 04, 01),  LocalDate.of(2020, 02, 05) ,new BigDecimal(250) ,new BigDecimal(520),new BigDecimal(450),new BigDecimal(550)));
	
		
		when(changeRepo.findByRange(peoId, start, end)).thenReturn(changeList);
		
		AssetsService service= new AssetsServiceImpl(totalRepo, changeRepo);
		
		AssetsChanges changes = service.sumAssetChanges(peoId, start, end, "month");
		
		assertEquals(220.0, changes.getContributions().doubleValue());
		
	}
	
	
	@Test
	public void test_sum_assets_same_month_latest_date_available() {
		
		String peoId = "test";
		LocalDate start = LocalDate.of(2020, 01, 01);	
		LocalDate end = LocalDate.of(2020, 04, 30);	
		
	List<AssetsChanges> changeList = new ArrayList<>();
		
//		changeList.add(new AssetsChanges(peoId, LocalDate.of(2020, 04, 01),  LocalDate.of(2020, 04, 30) ,new BigDecimal(220) ,new BigDecimal(520),new BigDecimal(450),new BigDecimal(550)));
		changeList.add(new AssetsChanges(peoId,  LocalDate.of(2020, 04, 01),  LocalDate.of(2020, 04, 29) ,new BigDecimal(215) ,new BigDecimal(520),new BigDecimal(450),new BigDecimal(550)));
		changeList.add(new AssetsChanges(peoId,  LocalDate.of(2020, 04, 01),  LocalDate.of(2020, 01, 01) ,new BigDecimal(200) ,new BigDecimal(520),new BigDecimal(450),new BigDecimal(550)));
		changeList.add(new AssetsChanges(peoId,  LocalDate.of(2020, 04, 01),  LocalDate.of(2020, 02, 05) ,new BigDecimal(200) ,new BigDecimal(520),new BigDecimal(450),new BigDecimal(550)));
	
		
		when(changeRepo.findByRange(peoId, start, end)).thenReturn(changeList);
		
		AssetsService service= new AssetsServiceImpl(totalRepo, changeRepo);
		
		AssetsChanges changes = service.sumAssetChanges(peoId, start, end, "");
		
		assertEquals(215.0, changes.getContributions().doubleValue());
		
	}
	
	@Test
	public void test_sum_assets_multiple_months() {
		
		String peoId = "test";
		LocalDate start = LocalDate.of(2020, 01, 01);	
		LocalDate end = LocalDate.of(2020, 04, 29);	
		
	List<AssetsChanges> changeList = new ArrayList<>();
		
		changeList.add(new AssetsChanges(peoId, LocalDate.of(2020, 04, 01),  LocalDate.of(2020, 04, 30) ,new BigDecimal(220) ,new BigDecimal(520),new BigDecimal(450),new BigDecimal(550)));
		changeList.add(new AssetsChanges(peoId,  LocalDate.of(2020, 04, 01),  LocalDate.of(2020, 04, 29) ,new BigDecimal(220) ,new BigDecimal(520),new BigDecimal(450),new BigDecimal(550)));
		changeList.add(new AssetsChanges(peoId,  LocalDate.of(2020, 01, 01),  LocalDate.of(2020, 01, 31) ,new BigDecimal(250) ,new BigDecimal(520),new BigDecimal(450),new BigDecimal(550)));
		changeList.add(new AssetsChanges(peoId,  LocalDate.of(2020, 02, 01),  LocalDate.of(2020, 02, 29) ,new BigDecimal(250) ,new BigDecimal(520),new BigDecimal(450),new BigDecimal(550)));
		changeList.add(new AssetsChanges(peoId,  LocalDate.of(2020, 03, 01),  LocalDate.of(2020, 03, 31) ,new BigDecimal(250) ,new BigDecimal(520),new BigDecimal(450),new BigDecimal(550)));
		changeList.add(new AssetsChanges(peoId,  LocalDate.of(2020, 02, 01),  LocalDate.of(2020, 02, 02) ,new BigDecimal(50) ,new BigDecimal(520),new BigDecimal(450),new BigDecimal(550)));
		changeList.add(new AssetsChanges(peoId,  LocalDate.of(2020, 01, 01),  LocalDate.of(2020, 02, 29) ,new BigDecimal(100) ,new BigDecimal(520),new BigDecimal(450),new BigDecimal(550)));

		
		when(changeRepo.findByRange(peoId, start, end)).thenReturn(changeList);
		
		AssetsService service= new AssetsServiceImpl(totalRepo, changeRepo);
		
		AssetsChanges changes = service.sumAssetChanges(peoId, start, end, "month");
		
		assertEquals(970.0, changes.getContributions().doubleValue());
		
	}
	
	
	//Asset we are getting total assets happy path :)
	@Test
	public void test_assets_found_return_values() {
		
		String peoId = "test";
		LocalDate date = LocalDate.of(2020, 04, 03);	
		BigDecimal total = new BigDecimal(2_345_345.98);
		when(totalRepo.findByPeoIdAndDate(peoId, date)).thenReturn(Optional.of(new AssetsTotal(peoId,date,total)) );
		
		AssetsService service= new AssetsServiceImpl(totalRepo,changeRepo);

		AssetsTotal assets =service.getAssetsTotal(peoId, date);
		
		assertEquals(total.doubleValue(), assets.getTotal().doubleValue());
		assertEquals("test", assets.getPeoId());
		
		
	}
	
	
	//assert empty assets changs return zero values and NONE for peoId
	@Test
	public void test_changes_not_found() {
		String peoId = "test";
		LocalDate start = LocalDate.of(2020, 04, 01);	
		LocalDate end = LocalDate.of(2020, 04, 30);	
	
		when(changeRepo.findByPeoIdAndStartAndEnd(peoId, start,end)).thenReturn(Optional.of(new AssetsChanges()) );
		
		AssetsService service= new AssetsServiceImpl(totalRepo, changeRepo);

		AssetsChanges changes = service.getAssetsChange(peoId, start, end);
	
		assertEquals(BigDecimal.ZERO.doubleValue(), changes.getContributions().doubleValue());
		assertEquals("NONE", changes.getPeoId());
		
	}
	
	//assert empty return returns zeros for test peo id as NONE
	@Test
	public void test_assets_not_found_return_empty() {
		
		
		String peoId = "test2";
		LocalDate date = LocalDate.of(2020, 04, 03);		
		when(totalRepo.findByPeoIdAndDate(peoId, date)).thenReturn(Optional.of(new AssetsTotal()) );
		
		AssetsService service= new AssetsServiceImpl(totalRepo,changeRepo);

		AssetsTotal assets =service.getAssetsTotal(peoId, date);
		
		assertEquals(BigDecimal.ZERO.doubleValue(), assets.getTotal().doubleValue());
		assertEquals("NONE", assets.getPeoId());
		
		
	}
	

	@Test
	public void test_sum_assets_changes_years() {
		
		String peoId = "test";
		LocalDate start = LocalDate.of(2019, 01, 01);	
		LocalDate end = LocalDate.of(2019, 04, 30);	
		
		List<AssetsChanges> changeList = new ArrayList<>();
		
		changeList.add(new AssetsChanges(peoId, LocalDate.of(2019, 01, 01),  LocalDate.of(2019, 04, 30) ,new BigDecimal(220) ,new BigDecimal(520),new BigDecimal(450),new BigDecimal(500)));
		changeList.add(new AssetsChanges(peoId,  LocalDate.of(2020, 04, 01),  LocalDate.of(2020, 04, 30) ,new BigDecimal(220) ,new BigDecimal(520),new BigDecimal(450),new BigDecimal(550)));

		
		when(changeRepo.findAssetChangesPerYear(peoId, start, end, 2)).thenReturn(changeList);
		
		when(changeRepo.findByRange(peoId, start, end)).thenReturn(changeList);		
		
		AssetsService service= new AssetsServiceImpl(totalRepo, changeRepo);
		
		AssetsChanges changes = service.sumAssetChanges(peoId, start, end, "year");
		
		assertEquals(440.0, changes.getContributions().doubleValue());
		assertEquals(1040.0, changes.getDistributions().doubleValue());
		assertEquals(900.0, changes.getTransferIn().doubleValue());
		assertEquals(1050.0, changes.getTransferOut().doubleValue());
		
	}
	
	@Test
	public void test_sum_assets_changes_quarter() {
		
		String peoId = "test";
		LocalDate start = LocalDate.of(2019, 01, 01);	
		LocalDate end = LocalDate.of(2019, 04, 30);	
		
		List<AssetsChanges> changeList = new ArrayList<>();
		
		changeList.add(new AssetsChanges(peoId, LocalDate.of(2019, 01, 01),  LocalDate.of(2019, 04, 30) ,new BigDecimal(20) ,new BigDecimal(50),new BigDecimal(45),new BigDecimal(550)));
		changeList.add(new AssetsChanges(peoId,  LocalDate.of(2020, 04, 01),  LocalDate.of(2020, 04, 29) ,new BigDecimal(20) ,new BigDecimal(50),new BigDecimal(45),new BigDecimal(550)));

		when(changeRepo.findAssetsChangesByQuarter(peoId, start, end, 2)).thenReturn(changeList);
		
		when(changeRepo.findByRange(peoId, start, end)).thenReturn(changeList);
		
		AssetsService service= new AssetsServiceImpl(totalRepo, changeRepo);
		
		AssetsChanges changes = service.sumAssetChanges(peoId, start, end, "quarter");
		
		assertEquals(40.0, changes.getContributions().doubleValue());
		assertEquals(100.0, changes.getDistributions().doubleValue());
		assertEquals(90.0, changes.getTransferIn().doubleValue());
		assertEquals(1100.0, changes.getTransferOut().doubleValue());
	}
	
	@Test
	public void test_assets_changes_year() {
		
		String peoId = "test";
		LocalDate start = LocalDate.of(2019, 01, 01);	
		LocalDate end = LocalDate.of(2019, 04, 30);	
		
		List<AssetsChanges> changeList = new ArrayList<>();
		
		changeList.add(new AssetsChanges(peoId, LocalDate.of(2019, 01, 01),  LocalDate.of(2019, 04, 30) ,new BigDecimal(220) ,new BigDecimal(520),new BigDecimal(450),new BigDecimal(550)));
		changeList.add(new AssetsChanges(peoId,  LocalDate.of(2020, 04, 01),  LocalDate.of(2020, 04, 29) ,new BigDecimal(220) ,new BigDecimal(520),new BigDecimal(450),new BigDecimal(550)));

		AssetsTotal assetTotal = new AssetsTotal("test", start, new BigDecimal(400));
				
		Mockito.when(totalRepo.findByPeoIdAndDate(peoId, start)).thenReturn(Optional.of(assetTotal));
				
		Mockito.when(totalRepo.findByPeoIdAndDate(peoId, end)).thenReturn(Optional.of(new AssetsTotal(peoId,end,new BigDecimal(500))));
		
		//Mockito.when(changeRepo.findByPeoIdAndStartAndEnd(peoId, start, end)).thenReturn(Optional.empty());
		Mockito.when(changeRepo.findByPeoIdAndStartAndEnd(peoId, start,end)).thenReturn(Optional.of(changeList.get(0)));
		
		Mockito.when(changeRepo.findByRange(peoId, start, end)).thenReturn(changeList);
		
		Mockito.when(changeRepo.findAssetChangesPerYear(peoId, start, end, 2)).thenReturn(changeList);
		
		
		
		AssetsService service= new AssetsServiceImpl(totalRepo, changeRepo);
		AssetsDTO dto  = service.getAssetsStats(peoId, start, end, "year", 1);
		double changes = dto.getChanges().getContributions();
		assertEquals(220.0, changes);
	}
	
	@Test
	public void test_assets_changes_quarter() {
		
		String peoId = "test";
		LocalDate start = LocalDate.of(2019, 01, 01);	
		LocalDate end = LocalDate.of(2020, 04, 30);	
		
		LocalDate start2 = LocalDate.of(2020, 04, 01);
		
		List<AssetsChanges> changeList = new ArrayList<>();
		
		changeList.add(new AssetsChanges(peoId, LocalDate.of(2019, 01, 01),  LocalDate.of(2019, 04, 30) ,new BigDecimal(220) ,new BigDecimal(520),new BigDecimal(450),new BigDecimal(550)));
		changeList.add(new AssetsChanges(peoId,  LocalDate.of(2020, 04, 01),  LocalDate.of(2020, 04, 30) ,new BigDecimal(220) ,new BigDecimal(520),new BigDecimal(450),new BigDecimal(550)));

		//start
		Mockito.when(totalRepo.findByPeoIdAndDate(peoId, start)).thenReturn(Optional.of(new AssetsTotal(peoId,start,new BigDecimal(300))));	
		//end
		Mockito.when(totalRepo.findByPeoIdAndDate(peoId, end)).thenReturn(Optional.of(new AssetsTotal(peoId,end,new BigDecimal(500))));
		//main changes		
		Mockito.when(changeRepo.findByPeoIdAndStartAndEnd(peoId, start,end)).thenReturn(Optional.of(changeList.get(0)));//should b null
		//sum of asset changes
		Mockito.when(changeRepo.findByRange(peoId, start, end)).thenReturn(changeList);
		//assets by interval quarter		
		Mockito.when(changeRepo.findAssetsChangesByQuarter(peoId, start, end, 1)).thenReturn(changeList);
		
		///end
		Mockito.when(totalRepo.findByPeoIdAndDate(peoId, start2)).thenReturn(Optional.of(new AssetsTotal(peoId,start,new BigDecimal(200))));
		
		
		AssetsService service= new AssetsServiceImpl(totalRepo, changeRepo);
		AssetsDTO dto  = service.getAssetsStats(peoId, start, end, "quarter", 3);
		double changes = dto.getChanges().getContributions();
		assertEquals(220.0, changes);

	}
	
	@Test
	public void test_sum_assets_changes_year() {
		
		String peoId = "test";
		LocalDate start = LocalDate.of(2019, 01, 01);	
		LocalDate end = LocalDate.of(2019, 04, 30);	
		
		List<AssetsChanges> changeList = new ArrayList<>();
		
		changeList.add(new AssetsChanges(peoId, LocalDate.of(2019, 01, 01),  LocalDate.of(2019, 04, 30) ,new BigDecimal(20) ,new BigDecimal(50),new BigDecimal(45),new BigDecimal(550)));
		changeList.add(new AssetsChanges(peoId,  LocalDate.of(2020, 04, 01),  LocalDate.of(2020, 04, 29) ,new BigDecimal(20) ,new BigDecimal(50),new BigDecimal(45),new BigDecimal(550)));

		when(changeRepo.findAssetChangesPerYear(peoId, start, end, 1)).thenReturn(changeList);
		
		when(changeRepo.findByRange(peoId, start, end)).thenReturn(changeList);
		
		AssetsService service= new AssetsServiceImpl(totalRepo, changeRepo);
		
		AssetsChanges changes = service.sumAssetChanges(peoId, start, end, "year");
		
		assertEquals(40.0, changes.getContributions().doubleValue());
		assertEquals(100.0, changes.getDistributions().doubleValue());
		assertEquals(90.0, changes.getTransferIn().doubleValue());
		assertEquals(1100.0, changes.getTransferOut().doubleValue());
	}
	
}
