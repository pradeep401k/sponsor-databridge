package com.slavic.webapp.service;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

import java.sql.Date;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import org.junit.jupiter.api.Test;
import org.mockito.Mock;
import org.mockito.Mockito;

import com.slavic.springboot.dto.AverageDeferralDTO;
import com.slavic.springboot.mysql.model.AverageDeferral;
import com.slavic.springboot.mysql.repository.AverageDeferralRepository;
import com.slavic.springboot.service.AverageDeferralService;
import com.slavic.springboot.service.impl.AverageDeferralServiceImpl;

//import com.slavic.springboot.dto.AverageDeferralDTO;
//import com.slavic.springboot.dto.AverageDeferralDTO.RatesByInterval;
//import com.slavic.springboot.mysql.model.AverageDeferral;
//import com.slavic.springboot.mysql.repository.AverageDeferralRepository;
//import com.slavic.webapp.service.impl.AverageDeferralServiceImpl;

class AverageDeferralServiceTest {

	private AverageDeferralRepository averageDeferralRepository = mock(AverageDeferralRepository.class);
	
	
	@Test
	public void happyPath() {
		
		String peoId = "test";
		LocalDate start = LocalDate.of(2020, 04, 01);	
		LocalDate end = LocalDate.of(2020, 04, 30);	
		String interval = "month";
		
		List<AverageDeferral> list = new ArrayList<>();
		
		list.add(new AverageDeferral(peoId,LocalDate.of(2020, 04, 01),LocalDate.of(2020, 04, 30),6));
		list.add(new AverageDeferral(peoId,LocalDate.of(2020, 04, 29),LocalDate.of(2020, 04, 30),5.98));
		list.add(new AverageDeferral(peoId,LocalDate.of(2020, 04, 28),LocalDate.of(2020, 04, 30),5.98));
		list.add(new AverageDeferral(peoId,LocalDate.of(2020, 05, 31),LocalDate.of(2020, 04, 30),6));
		list.add(new AverageDeferral(peoId,LocalDate.of(2020, 03, 31),LocalDate.of(2020, 04, 30),6));
		list.add(new AverageDeferral(peoId,LocalDate.of(2020, 02, 29),LocalDate.of(2020, 04, 30),6));
		list.add(new AverageDeferral(peoId,LocalDate.of(2020, 01, 31),LocalDate.of(2020, 04, 30),5));
		list.add(new AverageDeferral(peoId,LocalDate.of(2020, 12, 31),LocalDate.of(2020, 04, 30),5));
		list.add(new AverageDeferral(peoId,LocalDate.of(2020, 11, 30),LocalDate.of(2020, 04, 30),3));
		list.add(new AverageDeferral(peoId,LocalDate.of(2020, 10, 31),LocalDate.of(2020, 04, 30),3));
	
		AverageDeferral startAvg =new AverageDeferral(peoId,start,start,5.68);
		AverageDeferral endAvg = new AverageDeferral(peoId,start,end,6.01);
		
		AverageDeferralService service = new AverageDeferralServiceImpl(averageDeferralRepository);
		
		when(averageDeferralRepository.findByPeoIdAndStartDateAndEndDate(peoId, start, start)).thenReturn(Optional.of(startAvg));
		when(averageDeferralRepository.findByPeoIdAndStartDateAndEndDate(peoId,start, end)).thenReturn(Optional.of(endAvg));
		when(averageDeferralRepository.findByDateRange(peoId, start, end)).thenReturn(list);
		
		AverageDeferralDTO  dto =service.getAverageDeferralStats(peoId, start, end, interval, 3);
		//assert dto start avg is correct
		assertEquals(5.68,dto.getStart().getRate());
		//assert dto end avg is correct
		assertEquals(6.01,dto.getEnd().getRate());
		
		//assert interval is monthly and only end of months date
		assertEquals(3,dto.getRatesByInterval().size());

	}
	
	@Test
	public void yearlyDeferrals() {
		String peoId = "test";
		LocalDate start = LocalDate.of(2019, 01, 01);
		LocalDate end = LocalDate.of(2020, 04, 01);
		Date sqlDate = Date.valueOf(start);
		int limit = 2;
		
		AverageDeferralDTO dto = new AverageDeferralDTO();
		List<AverageDeferralDTO.RatesByInterval> ratesList = new ArrayList<>(); // list RatesByInterval
		
		AverageDeferral avgDef = new AverageDeferral(peoId, start,end, 100);
		AverageDeferral avgDe2 = new AverageDeferral(peoId, start,end, 200);
		
		List<AverageDeferral> list = new ArrayList<>();	
		list.add(avgDef);
		list.add(avgDe2);
		
		AverageDeferralDTO.RatesByInterval rbi = dto.new RatesByInterval(sqlDate,100);
		ratesList.add(rbi);
		AverageDeferralDTO.RatesByInterval rbi2 = dto.new RatesByInterval(sqlDate,200);
		ratesList.add(rbi2);
		
		dto.setRatesByInterval(ratesList);
		
		//start avg		
		 Mockito.when(averageDeferralRepository.findByPeoIdAndStartDateAndEndDate(peoId, start,start)).thenReturn(Optional.of(avgDef));
		//end avg		
		 Mockito.when(averageDeferralRepository.findByPeoIdAndStartDateAndEndDate(peoId,start, end)).thenReturn(Optional.of(avgDef));
		 //quarterly list
		 Mockito.when(averageDeferralRepository.findByYearRange(peoId, start, end, limit)).thenReturn(list);
		 
		 AverageDeferralService service = new AverageDeferralServiceImpl(averageDeferralRepository);
		 
		 dto = service.getAverageDeferralStats(peoId, start, end, "year", limit); 
		 ratesList = dto.getRatesByInterval();
		 rbi =ratesList.get(0);
		 rbi2 = ratesList.get(1);
		 
		 assertEquals(rbi.getRate(),100);
		 assertEquals(rbi2.getRate(),200);
	}
	
	@Test
	public void quarterlylyDeferrals() {
		String peoId = "test";
		LocalDate start = LocalDate.of(2017, 01, 01);
		LocalDate end = LocalDate.of(2020, 04, 01);
		Date sqlDate = Date.valueOf(start);
		
		AverageDeferralDTO dto = new AverageDeferralDTO();
		List<AverageDeferralDTO.RatesByInterval> ratesList = new ArrayList<>(); // list RatesByInterval
		
		AverageDeferral avgDef = new AverageDeferral(peoId, start,end, 200);
		AverageDeferral avgDe2 = new AverageDeferral(peoId, start,end, 400);
		
		List<AverageDeferral> list = new ArrayList<>();	
		list.add(avgDef);
		list.add(avgDe2);
		
		//AverageDeferralDTO.RatesByInterval rbi = dto.new RatesByInterval(sqlDate,200);
		//ratesList.add(rbi);
		//AverageDeferralDTO.RatesByInterval rbi2 = dto.new RatesByInterval(sqlDate,400);
		//ratesList.add(rbi2);
		
		//dto.setRatesByInterval(ratesList);
		
		//start avg		
		 Mockito.when(averageDeferralRepository.findByPeoIdAndStartDateAndEndDate(peoId, start,start)).thenReturn(Optional.of(avgDef));
		//end avg		
		 Mockito.when(averageDeferralRepository.findByPeoIdAndStartDateAndEndDate(peoId,start, end)).thenReturn(Optional.of(avgDef));
		 //quarterly list
		 Mockito.when(averageDeferralRepository.findByQuarterRange(peoId, start, end, 1)).thenReturn(list);
		 
		 AverageDeferralService service = new AverageDeferralServiceImpl(averageDeferralRepository);
		 
		 dto = service.getAverageDeferralStats(peoId, start, end, "quarter", 1); 
		 ratesList = dto.getRatesByInterval();
		 AverageDeferralDTO.RatesByInterval rbi =ratesList.get(0);
		 AverageDeferralDTO.RatesByInterval rbi2 = ratesList.get(1);
		 
		 assertEquals(rbi.getRate(),200);
		 assertEquals(rbi2.getRate(),400);
	}

	@Test
	public void monthlyDeferrals() {
		String peoId = "test";
		LocalDate start = LocalDate.of(2017, 01, 01);
		LocalDate end = LocalDate.of(2020, 04, 01);
		int limit = 2;
		
		List<AverageDeferral> list = new ArrayList<>();
		
		list.add(new AverageDeferral(peoId,LocalDate.of(2020, 04, 30),LocalDate.of(2020, 04, 30),6));
		list.add(new AverageDeferral(peoId,LocalDate.of(2020, 04, 29),LocalDate.of(2020, 04, 30),5.98));
		list.add(new AverageDeferral(peoId,LocalDate.of(2020, 04, 28),LocalDate.of(2020, 04, 30),5.98));
		list.add(new AverageDeferral(peoId,LocalDate.of(2020, 05, 31),LocalDate.of(2020, 04, 30),6));
		list.add(new AverageDeferral(peoId,LocalDate.of(2020, 03, 31),LocalDate.of(2020, 04, 30),6));
		list.add(new AverageDeferral(peoId,LocalDate.of(2020, 02, 29),LocalDate.of(2020, 04, 30),6));
		list.add(new AverageDeferral(peoId,LocalDate.of(2020, 01, 31),LocalDate.of(2020, 04, 30),5));
		list.add(new AverageDeferral(peoId,LocalDate.of(2020, 12, 31),LocalDate.of(2020, 04, 30),5));
		list.add(new AverageDeferral(peoId,LocalDate.of(2020, 11, 30),LocalDate.of(2020, 04, 30),3));
		list.add(new AverageDeferral(peoId,LocalDate.of(2020, 10, 31),LocalDate.of(2020, 04, 30),3));
		
		AverageDeferral avgDef = new AverageDeferral(peoId, start,end, 100);
		AverageDeferral avgDef2 = new AverageDeferral(peoId, start,end, 200);
		
		//start avg		
		 Mockito.when(averageDeferralRepository.findByPeoIdAndStartDateAndEndDate(peoId, start,start)).thenReturn(Optional.of(avgDef));
		//end avg		
		 Mockito.when(averageDeferralRepository.findByPeoIdAndStartDateAndEndDate(peoId, start,end)).thenReturn(Optional.of(avgDef2));
		 //quarterly list
		 Mockito.when(averageDeferralRepository.findByDateRange(peoId, start, end)).thenReturn(list);
		 
		 AverageDeferralService service = new AverageDeferralServiceImpl(averageDeferralRepository);
		 
		 AverageDeferralDTO  dto =service.getAverageDeferralStats(peoId, start, end, "month", limit);
		
		 AverageDeferralDTO.RatesByInterval rbi = dto.getRatesByInterval().get(0);
		 assertEquals(rbi.getRate(),6);
	}
	
	
}
